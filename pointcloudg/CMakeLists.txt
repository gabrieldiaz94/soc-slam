cmake_minimum_required(VERSION 2.8.3)
project(pointcloudg)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  pcl_conversions
  pcl_msgs
  pcl_ros
  sensor_msgs
)
find_package(PCL REQUIRED)
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES chapter6_tutorials
#  CATKIN_DEPENDS pcl_conversions pcl_msgs pcl_ros sensor_msgs
#  DEPENDS system_lib
)
include_directories(
  include ${catkin_INCLUDE_DIRS}include ${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})

include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)

add_compile_options(-std=c++11)

add_executable(datapoint
  src/datapoint.cpp
)
add_dependencies(datapoint ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(datapoint
  ${catkin_LIBRARIES}
)

add_executable(realtime
  src/realtime.cpp
)
add_dependencies(realtime ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(realtime
  ${catkin_LIBRARIES}
)


add_executable(dataanalisis
  src/dataanalisis.cpp
)
add_dependencies(dataanalisis ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(dataanalisis
  ${catkin_LIBRARIES}
)

add_executable(ejemplo
  src/ejemplo.cpp
)
add_dependencies(ejemplo ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(ejemplo
  ${catkin_LIBRARIES}
)

add_executable(realtime1
  src/realtime1.cpp
)
add_dependencies(realtime1 ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(realtime1
  ${catkin_LIBRARIES}
)
add_executable(get_time_map
  src/get_time_map.cpp
)
add_dependencies(get_time_map ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(get_time_map
  ${catkin_LIBRARIES}
)

add_executable(tfdata
  src/tfdata.cpp
)
add_dependencies(tfdata ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(tfdata
  ${catkin_LIBRARIES}
)
add_executable(accurancygrid
  src/accurancygrid.cpp
)
add_dependencies(accurancygrid ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(accurancygrid
  ${catkin_LIBRARIES}
)
