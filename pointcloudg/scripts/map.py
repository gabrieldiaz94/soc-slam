#!/usr/bin/env python
import rospy
import open3d as o3d
import numpy as np
from std_msgs.msg import String

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("chatter", String, callback)
 #   ply_file = o3d.io.read_point_cloud("willowgarage_collision.ply")
    ply_file = o3d.io.read_point_cloud("ENDMAP.pcd")
    #ply_file.scale(1 / np.max(ply_file.get_max_bound() - ply_file.get_min_bound()),center=ply_file.get_center())
    ply_file.colors = o3d.utility.Vector3dVector(np.random.uniform(0, 1, size=(2000, 3)))
    o3d.visualization.draw_geometries([ply_file])
    print('voxelization')
    voxel_grid = o3d.geometry.VoxelGrid.create_from_point_cloud(ply_file,voxel_size=0.05)
    o3d.visualization.draw_geometries([voxel_grid])
    queries = np.asarray(ply_file.points)
    output = voxel_grid.check_if_included(o3d.utility.Vector3dVector(queries))
    print(output[:10])
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
