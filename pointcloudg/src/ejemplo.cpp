#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>
#include <nav_msgs/Odometry.h>
#include "sensor_msgs/Imu.h"
#define PI 3.14159265
struct Puntos{
 double  x, y,z;
 int n,val,cuadranteC,cuadranteE;
};


int carrito=0;
int carrito1=0;
sensor_msgs::PointCloud2 entrada;
void cloudCB(const sensor_msgs::PointCloud2 &input)
{ 
    entrada=input;
    carrito=1;    
}


main (int argc, char **argv)
{
	ros::init (argc, argv, "read_data");
	ros::NodeHandle nh;
	ros::Subscriber bat_sub=nh.subscribe("/rslidar_points",64,cloudCB);
    //ros::Subscriber bat_sub=nh.subscribe("/rs_points",64,cloudCB);
    ros::Publisher pcl_pub2=nh.advertise<sensor_msgs::PointCloud2>("voxelcloud",1);
  //  ros::spin();
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    ros::Rate loop_rate(100);
    int contador=0;
    int max=0;
    int min=64000;
    while(ros::ok())
	{   
	    if(carrito==1){  
            pcl::fromROSMsg(entrada,*cloud);
	    if(max<cloud->points.size ()){
                max=cloud->points.size ();
		std::cout<<"1: "<<cloud->points.size ()<<std::endl;
	       
	    }
	    if(min>cloud->points.size ()){
                min=cloud->points.size ();
		std::cout<<"2: "<<cloud->points.size ()<<std::endl;
	       
	    }
	    //std::cout<<"2: "<<cloud->points.size ()<<std::endl;
	    if(contador==0){
            	pcl::io::savePCDFileASCII ("PruebasLIDAR2.pcd", *cloud);
	    }
            contador++;
	    }
	    ros::spinOnce();
	    loop_rate.sleep();
        }
		
	return 0;
}
