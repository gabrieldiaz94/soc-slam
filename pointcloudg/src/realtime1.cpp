#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>
#include <nav_msgs/Odometry.h>
#include "sensor_msgs/Imu.h"
#define PI 3.14159265
struct Puntos{
 double  x, y,z;
 int n,val,cuadranteC,cuadranteE;
};
struct Voxel{
 double valores [21845][3];
 double numeroobjeto [21845];
 int cantidad;
}Cubo,Cubo2,Cubo3;
struct slamc{
    double movimientocarro[21845][3];
    int maximosCoord[8191][4];
    int cantidad;
}Carro;
nav_msgs::Odometry odome;

Puntos* limites(Puntos* punto,int n,double maxX,double minX,double maxY, double minY, int maxZ, int minZ){
    Puntos* limits=new Puntos[n];
    int ii=0;
    for (int i=0;i<n;i++){
        if((punto[i].x>minX && punto[i].x<=maxX) && (punto[i].y>minY && punto[i].y<=maxY) && (punto[i].z>minZ && punto[i].z<=maxZ)){
            limits[ii].x=punto[i].x;
            limits[ii].y=punto[i].y;
            limits[ii].z=punto[i].z;
            limits[ii].val=0;
            ii=ii+1;
        }
    }
    limits[0].n=ii;
    return limits;
}
double sec_finquadrant;
int** gruposorg(int n,int puntos1[][3],int m,int ycuad,int xcuad,int zcuad){
    int** vectores = 0;
    int cnuevar=0,cnuevar1,cnuevar2=0,nuevar[n][3],nuevar1[n][3],nuevar2[n][4];
    int varx,vary,varz;
    double diffx,diffy,diffz;
    for(int i=0;i<n;i++){
        nuevar[cnuevar][0]=puntos1[i][0];
        nuevar[cnuevar][1]=puntos1[i][1];
        nuevar[cnuevar][2]=puntos1[i][2];
        nuevar1[cnuevar][0]=puntos1[i][0];
        nuevar1[cnuevar][1]=puntos1[i][1];
        nuevar1[cnuevar][2]=puntos1[i][2];
        cnuevar=cnuevar+1;
    }
    cnuevar1=cnuevar;
    int ii=0;
    while(cnuevar1>0){
        nuevar2[cnuevar2][0]=nuevar[ii][0];
        nuevar2[cnuevar2][1]=nuevar[ii][1];
        nuevar2[cnuevar2][2]=nuevar[ii][2];
        nuevar2[cnuevar2][3]=1;
        varx=nuevar[ii][0];
        vary=nuevar[ii][1];
        varz=nuevar[ii][2];
        cnuevar1=cnuevar;
        cnuevar=0;
        for(int j=0;j<cnuevar1;j++){
            if(ii!=j){
                diffx=sqrt(pow(varx-nuevar1[j][0],2));
                diffy=sqrt(pow(vary-nuevar1[j][1],2));
                diffz=sqrt(pow(varz-nuevar1[j][2],2));
                if(diffx<xcuad && diffy<ycuad && diffz<zcuad){
                    nuevar2[cnuevar2][0]=nuevar2[cnuevar2][0]+nuevar1[j][0];
                    nuevar2[cnuevar2][1]=nuevar2[cnuevar2][1]+nuevar1[j][1];
                    nuevar2[cnuevar2][2]=nuevar2[cnuevar2][2]+nuevar1[j][2];
                    nuevar2[cnuevar2][3]=nuevar2[cnuevar2][3]+1;
                }else{
                    nuevar[cnuevar][0]=nuevar1[j][0];
                    nuevar[cnuevar][1]=nuevar1[j][1];
                    nuevar[cnuevar][2]=nuevar1[j][2];
                    cnuevar=cnuevar+1;
                }
            }
        }
        nuevar2[cnuevar2][0]=nuevar2[cnuevar2][0]/nuevar2[cnuevar2][3];
        nuevar2[cnuevar2][1]=nuevar2[cnuevar2][1]/nuevar2[cnuevar2][3];
        nuevar2[cnuevar2][2]=nuevar2[cnuevar2][2]/nuevar2[cnuevar2][3];
        cnuevar2=cnuevar2+1;
        for(int j=0;j<cnuevar;j++){
            nuevar1[j][0]=nuevar[j][0];
            nuevar1[j][1]=nuevar[j][1];
            nuevar1[j][2]=nuevar[j][2];
        }  
    }
    vectores = new int*[cnuevar2+1];
    for(int i=0;i<cnuevar2+1;i++){
        vectores[i] = new int[3];
        if(i==0){
            vectores[i][0]=cnuevar2+1;
            vectores[i][1]=cnuevar2+1;
            vectores[i][2]=cnuevar2+1;
        }else{
            vectores[i][0]=nuevar2[i-1][0];
            vectores[i][1]=nuevar2[i-1][1];
            vectores[i][2]=nuevar2[i-1][2];
        }
    }
    return vectores;
}
Puntos* organizar(Puntos* puntos, int n,double x, double y, double z,int numobs){
    int xx,yy,zz,cuad1=0,cuad2=0,cuad3=0,cuad4=0;
    int xcuad=x*1000,ycuad=y*1000,zcuad=z*1000;
    int cuadrante1[n][3],cuadrante2[n][3],cuadrante3[n][3],cuadrante4[n][3];
    for(int i=0;i<n;i++){
        xx=puntos[i].x*1000;
        yy=puntos[i].y*1000;
        zz=puntos[i].z*1000;
        if(zz>-380){
            if(xx>=0 && yy>=0 ){
                cuadrante1[cuad1][0]=xx;
                cuadrante1[cuad1][1]=yy;
                cuadrante1[cuad1][2]=zz;
                cuad1=cuad1+1;
            }
            if(xx<0 && yy>=0 ){
                cuadrante2[cuad2][0]=xx;
                cuadrante2[cuad2][1]=yy;
                cuadrante2[cuad2][2]=zz;
                cuad2=cuad2+1;
            }
            if(xx<0 && yy<0 ){
                cuadrante3[cuad3][0]=xx;
                cuadrante3[cuad3][1]=yy;
                cuadrante3[cuad3][2]=zz;
                cuad3=cuad3+1;
            }
            if(xx>=0 && yy<0){
                cuadrante4[cuad4][0]=xx;
                cuadrante4[cuad4][1]=yy;
                cuadrante4[cuad4][2]=zz;
                cuad4=cuad4+1;
            }
        }
    }
 //   int** frente1= grupos(contar02,contar11,coord02,coord11);
    int anglec;
    sec_finquadrant=ros::Time::now().toSec();
    Puntos* puntosV=new Puntos[n];
    std::cout<<" cuad 1 size: "<<cuad1<<std::endl;
    std::cout<<" cuad 2 size: "<<cuad2<<std::endl;
    std::cout<<" cuad 3 size: "<<cuad3<<std::endl;
    std::cout<<" cuad 4 size: "<<cuad4<<std::endl;
    int iis=0;
    if(cuad1>0){
        anglec=1;
        int** nuevofrente1=gruposorg(cuad1,cuadrante1,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente1[0][0];i++){
	//	if(iis<256){
                        puntosV[iis].x=(double)  nuevofrente1[i][0]/1000;
                        puntosV[iis].y= (double) nuevofrente1[i][1]/1000;
                        puntosV[iis].z= (double) nuevofrente1[i][2]/1000;
                        iis=iis+1;
	//	}
        }
    }
    if(cuad2>0){
        anglec=-1;
        int** nuevofrente2=gruposorg(cuad2,cuadrante2,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente2[0][0];i++){
	//	if(iis<256){                 
		        puntosV[iis].x=(double)  nuevofrente2[i][0]/1000;
                        puntosV[iis].y= (double) nuevofrente2[i][1]/1000;
                        puntosV[iis].z= (double) nuevofrente2[i][2]/1000;
                        iis=iis+1;
	//	}
        }
    }
    if(cuad3>0){
        anglec=1;
        int** nuevofrente3=gruposorg(cuad3,cuadrante3,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente3[0][0];i++){
	//	if(iis<256){
                        puntosV[iis].x=(double)  nuevofrente3[i][0]/1000;
                        puntosV[iis].y= (double) nuevofrente3[i][1]/1000;
                        puntosV[iis].z= (double) nuevofrente3[i][2]/1000;
                        iis=iis+1;
	//	}
        }
    }
    if(cuad4>0){
        anglec=-1;
        int** nuevofrente4=gruposorg(cuad4,cuadrante4,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente4[0][0];i++){
	//		if(iis<256){
		                puntosV[iis].x=(double)  nuevofrente4[i][0]/1000;
		                puntosV[iis].y= (double) nuevofrente4[i][1]/1000;
		                puntosV[iis].z= (double) nuevofrente4[i][2]/1000;
		                iis=iis+1;
	//		}
        }
    }
    int nuevar[iis][4],cnuevar=0,nuevar1[iis][4],cnuevar1=0,nuevar2[iis][4],cnuevar2=0,conteo=2;
    float diffx,diffy,diffz,diffr,diffr1;
    int varx,vary,varz;
    for(int i=0;i<iis;i++){
        nuevar[cnuevar][0]=puntosV[i].x*1000;
        nuevar[cnuevar][1]=puntosV[i].y*1000;
        nuevar[cnuevar][2]=puntosV[i].z*1000;
        nuevar1[cnuevar][0]=puntosV[i].x*1000;
        nuevar1[cnuevar][1]=puntosV[i].y*1000;
        nuevar1[cnuevar][2]=puntosV[i].z*1000;
        cnuevar=cnuevar+1;
    }
    cnuevar1=cnuevar;
    int numobj=1+numobs;
    while(cnuevar1>0){
        for(int i=0;i<conteo;i++){
            if(i==0){
                varx=nuevar[i][0];
                vary=nuevar[i][1];
                varz=nuevar[i][2];
                nuevar2[cnuevar2][0]=nuevar[i][0];
                nuevar2[cnuevar2][1]=nuevar[i][1];
                nuevar2[cnuevar2][2]=nuevar[i][2];
                nuevar2[cnuevar2][3]=numobj;
                cnuevar2=cnuevar2+1;
            }else{
                varx=nuevar2[i][0];
                vary=nuevar2[i][1];
                varz=nuevar2[i][2];
            }
            cnuevar1=cnuevar;
            cnuevar=0;
            for(int j=0;j<cnuevar1;j++){
                if((i==0 && i!=j)||(i>0)){
                    diffx=sqrt(pow(varx-nuevar1[j][0],2));
                    diffy=sqrt(pow(vary-nuevar1[j][1],2));
                    diffz=sqrt(pow(varz-nuevar1[j][2],2));
                    if(diffx<=xcuad*4 && diffy<=ycuad*4 && diffz<=zcuad*4){
                        nuevar2[cnuevar2][0]=nuevar1[j][0];
                        nuevar2[cnuevar2][1]=nuevar1[j][1];
                        nuevar2[cnuevar2][2]=nuevar1[j][2];
                        nuevar2[cnuevar2][3]=numobj;
                        cnuevar2=cnuevar2+1;
                    } else{
                        nuevar[cnuevar][0]=nuevar1[j][0];
                        nuevar[cnuevar][1]=nuevar1[j][1];
                        nuevar[cnuevar][2]=nuevar1[j][2]; 
                        cnuevar=cnuevar+1; 
                    }             
                }
            }
            for(int j=0;j<cnuevar;j++){
                nuevar1[j][0]=nuevar[j][0];
                nuevar1[j][1]=nuevar[j][1];
                nuevar1[j][2]=nuevar[j][2]; 
            }
            if(cnuevar1==0){
                i=conteo;
            }
            conteo=cnuevar2;
        }
        numobj=numobj+1;
    }
    Puntos* puntosV1=new Puntos[cnuevar2];
    iis=0;
    for(int i=0;i<cnuevar2;i++){
        puntosV1[iis].x=(double) nuevar2[i][0]/1000;
        puntosV1[iis].y=(double) nuevar2[i][1]/1000;
        puntosV1[iis].z=(double) nuevar2[i][2]/1000;
        puntosV1[iis].val=nuevar2[i][3];
        iis=iis+1;
    }
    std::cout<<numobj-1<<std::endl;
    puntosV1[0].n=iis;
    return  puntosV1;
}
int carrito=0;
int carrito1=0;
sensor_msgs::PointCloud2 entrada;
void cloudCB(const sensor_msgs::PointCloud2 &input)
{ 
    entrada=input;
    carrito=1;    
}
double posicionx,posiciony;
double qz,qw,qy,qx,s,phi;
void odometria(const nav_msgs::Odometry &input){
    double posesx=(input.pose.pose.position.x)*100;
    double posesy=(input.pose.pose.position.y)*100;
    posicionx=input.pose.pose.position.x;
    posiciony=input.pose.pose.position.y;
    
    if(carrito1==0){
        carrito1=1;
    }  
}
void imudataread(const sensor_msgs::Imu  &input)
{
    double nx,ny,nz,ox,oy,oz,ax,ay,az;
    qz=input.orientation.z;
    qw=input.orientation.w;
    qy=input.orientation.y;
    qx=input.orientation.x;
    s=pow(sqrt(pow(qz,2)+pow(qw,2)+pow(qx,2)+pow(qy,2)),-2);
    nx=1-(2*s*(pow(qy,2)+pow(qz,2)));
    ny=2*s*((qx*qy)+(qz*qw));
    nz=2*s*((qx*qz)-(qy*qw));
    ox=2*s*((qx*qy)-(qz*qw));
    oy=1-(2*s*(pow(qx,2)+pow(qz,2)));
    oz=2*s*((qy*qz)+(qx*qw));
    ax=2*s*((qx*qz)+(qy*qw));
    ay=2*s*((qy*qz)-(qx*qw));
    az=1-(2*s*(pow(qx,2)+pow(qy,2)));
    if(qz<0){
        phi=atan2(sqrt(pow(oz-ay,2)+pow(ax-nz,2)+pow(ny-ox,2)),(nx+oy+az-1));
    }else{
        phi=-1*atan2(sqrt(pow(oz-ay,2)+pow(ax-nz,2)+pow(ny-ox,2)),(nx+oy+az-1));
    }
}
int cambio;
void llenarmapa(Puntos* puntos, int n,int posicionesx,int posicionesy){
    int puntosmov[n][3];
    for(int i=0;i<n;i++){
        if(puntos[i].z>-0.4){
            puntosmov[i][0]=(puntos[i].x*100)+posicionesx;
            puntosmov[i][1]=(puntos[i].y*100)+posicionesy;
            puntosmov[i][2]=(puntos[i].z*100);
        }
    }

}
double sec_now;
double sec_finlimit;
double sec_finvoxel;
double sec_fininitmap;
double sec_finvalpos;
double sec_finupdatepos;
main (int argc, char **argv)
{
    ros::init (argc, argv, "read_data");
    ros::NodeHandle nh;
    sec_now=ros::Time::now().toSec();
    phi=0;
    //ros::Subscriber bat_sub=nh.subscribe("/rslidar_points",64,cloudCB);
    ros::Subscriber bat_sub=nh.subscribe("/rs_points",64,cloudCB);
    ros::Subscriber bat_sub1=nh.subscribe("/odom",64,odometria);
    ros::Subscriber bat_sub2=nh.subscribe("/mobile_base/sensors/imu_data",64,imudataread);
    ros::Publisher pcl_pub2=nh.advertise<sensor_msgs::PointCloud2>("voxelcloud",1);
    ros::Publisher pcl_pub3=nh.advertise<sensor_msgs::PointCloud2>("limites",1);
  //  ros::spin();
    cambio=0;
    Cubo.cantidad=0;
    Cubo2.cantidad=0;
    Cubo3.cantidad=0;
    int iis=0;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
    sensor_msgs::PointCloud2 output3;
    sensor_msgs::PointCloud2 output2;
    double poseinicialx;
    double poseinicialy;
    double posefinalx=0;
    double posefinaly=0;
    int tamcloud;
    int conteovar1=0,conteovar2=0;
    int movX,movY;
    double diffx,diffy,diffx1,diffy1;
    double auxX,auxY,auxX1,auxY1;
    int auxi=0;
    Carro.cantidad=0;
    Puntos* punto;
    Puntos* puntoss;
    Puntos* puntosvoxel;
    double xvoxel=0.1,yvoxel=0.1,zvozel=0.1;
    int maxX=2,minX=-2,maxY=2,minY=-2;
    int maxZ=1;
    double maxXG[2],minXG[2],maxYG[2],minYG[2];
    maxXG[0]=5,minXG[0]=-5,maxYG[0]=5,minYG[0]=-5;
    double minZ=-1;
    double diffr,diffr1;
    double difftheta,difftheta1;
    int tamano;
	ros::Rate loop_rate(100);
    int alpha=0;
    int alpha1=0;
    int contador=0;
    int numerosobj=0;
    int numerosobj2=0,verificador;
    double movix1,movix2,moviy1,moviy2,moviz1,moviz2;
    int cantobj,cantobj2,contarobj;
    double mindr,mindr1,mindx,mindy,mindz;
    int mindx1,mindy1,mindz1;
    double phini;
    int cont,conta=0;
    int numindex[65535],indexc=0;
    int is;
    while(ros::ok())
	{ 
        if(carrito1==1){
            poseinicialx=(posicionx*cos(phi))-(posiciony*sin(phi));
            poseinicialy=(posiciony*cos(phi))+(posicionx*sin(phi));
          //  poseinicialx=posicionx;
          //  poseinicialy=posiciony;
            posefinalx=posicionx;
            posefinaly=posiciony;
            carrito1=2;
            phini=phi;
        }
        if(carrito==1 && (alpha==0 || alpha==2) && carrito1==2){
	    sec_now=ros::Time::now().toSec();
            pcl::fromROSMsg(entrada,*cloud);
            tamcloud=cloud->points.size ();
            punto =new Puntos[tamcloud];
	    is=0;
            for (int i=0;i<tamcloud;i++){
		conta=0;
		cloud->points[i].x==cloud->points[i].x? conta=0:
		conta=1;
                if(conta==0){
		punto[is].x=cloud->points[i].x;
		punto[is].y=cloud->points[i].y;
		punto[is].z=cloud->points[i].z; 
		is++;
		}
            }
            puntoss =new Puntos[is];
            puntosvoxel =new Puntos[is];
            puntoss=limites(punto,is,maxX,minX,maxY,minY,maxZ,minZ);
	    std::cout<<"Original CLoud: "<<is<<std::endl;
	    std::cout<<"Limites CLoud: "<<puntoss[0].n<<std::endl;
	    sec_finlimit=ros::Time::now().toSec(); 
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
            Carro.movimientocarro[contador][0]=posicionx;
            Carro.movimientocarro[contador][1]=posiciony;
            Carro.movimientocarro[contador][2]=phi;
            if(contador==0){
                puntosvoxel=organizar(puntoss,puntoss[0].n,xvoxel,yvoxel,zvozel,0);
		sec_finvoxel=ros::Time::now().toSec();
                tamano=puntosvoxel[0].n;
		std::cout<<"VOxel CLoud: "<<puntosvoxel[0].n<<std::endl;
                cloud3->width=tamano;
                cloud3->height=1;
                cloud3->points.resize (cloud3->width * cloud3->height);
                    for (int i=0; i<tamano;i++){
                            cloud3->points[i].x = puntosvoxel[i].x;
                            cloud3->points[i].y = puntosvoxel[i].y;
                            cloud3->points[i].z = puntosvoxel[i].z;
                            Cubo.valores[Cubo.cantidad][0]=puntosvoxel[i].x;
                            Cubo.valores[Cubo.cantidad][1]=puntosvoxel[i].y;
                            Cubo.valores[Cubo.cantidad][2]=puntosvoxel[i].z;
                            Cubo.numeroobjeto[Cubo.cantidad]=puntosvoxel[i].val;
                            Cubo.cantidad=Cubo.cantidad+1;
                            if(numerosobj<puntosvoxel[i].val){
                                numerosobj=puntosvoxel[i].val;
                            }
                    }
	        sec_fininitmap=ros::Time::now().toSec();
		std::cout<<"Limit execution time: "<<sec_finlimit-sec_now<<endl;
		std::cout<<"QUadrant Scatter execution time: "<<sec_finquadrant-sec_finlimit<<endl;
		std::cout<<"Voxel execution time: "<<sec_finvoxel-sec_finquadrant<<endl;
		std::cout<<"init Map execution time: "<<sec_fininitmap-sec_finvoxel<<endl;
            }else{
                puntosvoxel=organizar(puntoss,puntoss[0].n,xvoxel,yvoxel,zvozel,0);
		std::cout<<"VOxel CLoud: "<<puntosvoxel[0].n<<std::endl;
		sec_finvoxel=ros::Time::now().toSec();
                for(int i=0;i<puntosvoxel[0].n;i++){
                    if(numerosobj2<puntosvoxel[i].val){
                        numerosobj2=puntosvoxel[i].val;
                    }
                }
		std::cout<<"Limit execution time: "<<sec_finlimit-sec_now<<endl;
		std::cout<<"QUadrant Scatter execution time: "<<sec_finquadrant-sec_finlimit<<endl;
		std::cout<<"Voxel execution time: "<<sec_finvoxel-sec_finquadrant<<endl;
                diffx1=Carro.movimientocarro[contador][0] -  Carro.movimientocarro[contador-1][0];
                diffy1=Carro.movimientocarro[contador][1] -  Carro.movimientocarro[contador-1][1];
                difftheta=Carro.movimientocarro[contador][2]-Carro.movimientocarro[contador-1][2];
               // std::cout<<(phi*180)/PI<<std::endl;
                for(int i=0;i<Cubo.cantidad;i++){
                         diffx=(diffx1*cos(Carro.movimientocarro[contador][2]))-(diffy1*sin(Carro.movimientocarro[contador][2]));
                         diffy=(diffy1*cos(Carro.movimientocarro[contador][2]))+(diffx1*sin(Carro.movimientocarro[contador][2]));
                         diffx=(Cubo.valores[i][0]-diffx);
                         diffy=(Cubo.valores[i][1]-diffy);
                         Cubo.valores[i][0]=(diffx*cos(difftheta))-(diffy*sin(difftheta));
                         Cubo.valores[i][1]=(diffy*cos(difftheta))+(diffx*sin(difftheta));
                }
                conta=0;
                for(int i=0;i<contador;i++){
                    movix1=sqrt(pow(Carro.movimientocarro[contador][0]-Carro.movimientocarro[i][0],2));
                    moviy1=sqrt(pow(Carro.movimientocarro[contador][1]-Carro.movimientocarro[i][1],2));
                    if(movix1<0.01 && moviy1<0.01){
                        conta=1;
                    }
                }
		sec_finvalpos=ros::Time::now().toSec();
		std::cout<<"Val Pos execution time: "<<sec_finvalpos-sec_finvoxel<<endl;
                if(conta==1){
                    alpha=2;
                }
                if(alpha==0 && puntosvoxel[0].n>0){
                    indexc=0;
                    Cubo2.cantidad=0;
                    for(int i=0;i<puntosvoxel[0].n;i++){
                        movix1=puntosvoxel[i].x;
                        moviy1=puntosvoxel[i].y;
                        moviz1=puntosvoxel[i].z;
                        conta=0;
                        for(int j=0;j<Cubo.cantidad;j++){
                            movix2=sqrt(pow(movix1-Cubo.valores[j][0],2));
                            moviy2=sqrt(pow(moviy1-Cubo.valores[j][1],2));
                            moviz2=sqrt(pow(moviz1-Cubo.valores[j][2],2));
                            if(movix2<xvoxel-0.035 && moviy2<yvoxel-0.035 && moviz2<zvozel-0.035){
                                conta=1;
                                numindex[indexc]=j;
                                indexc=indexc+1;
                            }
                        }
                        if(conta==1){
                            Cubo2.valores[Cubo2.cantidad][0]=puntosvoxel[i].x;
                            Cubo2.valores[Cubo2.cantidad][1]=puntosvoxel[i].y;
                            Cubo2.valores[Cubo2.cantidad][2]=puntosvoxel[i].z;
                            Cubo2.cantidad=Cubo2.cantidad+1;
                        }else{
                            Cubo2.valores[Cubo2.cantidad][0]=puntosvoxel[i].x;
                            Cubo2.valores[Cubo2.cantidad][1]=puntosvoxel[i].y;
                            Cubo2.valores[Cubo2.cantidad][2]=puntosvoxel[i].z;
                            Cubo2.cantidad=Cubo2.cantidad+1;
                        }
                    }
                    for(int i=0;i<Cubo.cantidad;i++){
                        conta=0;
                        for(int j=0;j<indexc;j++){
                            if(numindex[j]==i){
                                conta=1;
                            }
                        }
                        if(conta==0){
                                Cubo2.valores[Cubo2.cantidad][0]=Cubo.valores[i][0];
                                Cubo2.valores[Cubo2.cantidad][1]=Cubo.valores[i][1];
                                Cubo2.valores[Cubo2.cantidad][2]=Cubo.valores[i][2];
                                Cubo2.cantidad=Cubo2.cantidad+1;
                        }
                    }
                    Cubo.cantidad=0;
                    for(int i=0;i<Cubo2.cantidad;i++){
                        Cubo.valores[Cubo.cantidad][0]=Cubo2.valores[i][0];
                        Cubo.valores[Cubo.cantidad][1]=Cubo2.valores[i][1];
                        Cubo.valores[Cubo.cantidad][2]=Cubo2.valores[i][2];
                        Cubo.cantidad=Cubo.cantidad+1;
                    }
		    sec_finupdatepos=ros::Time::now().toSec();
		    std::cout<<"Update Pos execution time: "<<sec_finupdatepos-sec_finvalpos<<endl;
                }
                tamano=Cubo.cantidad;
		if(puntosvoxel[0].n>0){
               //tamano=puntosvoxel[0].n;
                cloud3->width=tamano;
		std::cout<<tamano<<std::endl;
                cloud3->height=1;
		std::cout<<cloud3->height<<std::endl;
                cloud3->points.resize (cloud3->width * cloud3->height);
			for (int i=0; i<tamano;i++){
		                    cloud3->points[i].x = Cubo.valores[i][0];
		                    cloud3->points[i].y = Cubo.valores[i][1];
		                    cloud3->points[i].z = Cubo.valores[i][2];
		                   //cloud3->points[i].x= puntosvoxel[i].x;
		                   //cloud3->points[i].y= puntosvoxel[i].y;
		                   //cloud3->points[i].z= puntosvoxel[i].z;
		        }
		}
            }  	
            alpha1=alpha1+1;
            tamano=puntoss[0].n;
	    if(tamano>0){
		    cloud2->width=tamano;
		    cloud2->height=1;
		    cloud2->points.resize (cloud2->width * cloud2->height);
		    for (int i=0; i<tamano;i++){
		        cloud2->points[i].x = puntoss[i].x;
		        cloud2->points[i].y = puntoss[i].y;
		        cloud2->points[i].z = puntoss[i].z;
		    }
		    pcl::toROSMsg(*cloud2,output2);
            	    output2.header.frame_id =entrada.header.frame_id;
            	    pcl_pub3.publish(output2);
            }
	    if(puntosvoxel[0].n>0){
		    pcl::toROSMsg(*cloud3,output3);
		    output3.header.frame_id =entrada.header.frame_id;
		    pcl_pub2.publish(output3);    
	    }	
            alpha=1;
            //pcl::io::savePCDFileASCII ("PruebasCarro4.pcd", *cloud);
            contador++;

        }
        if(sqrt(pow(posefinalx-posicionx,2))>=0.1 || sqrt(pow(posefinaly-posiciony,2))>=0.1 || sqrt(pow(posefinalx-posicionx,2)+pow(posefinaly-posiciony,2))>0.07){
            poseinicialx=(posicionx*cos(phi))-(posiciony*sin(phi));
            poseinicialy=(posiciony*cos(phi))+(posicionx*sin(phi));
            phini=phi;
            //poseinicialx=posicionx;
            //poseinicialy=posiciony;
            posefinalx=posicionx;
            posefinaly=posiciony;
            alpha=0;
            std::cout<<"Cambio "<<std::endl; 
	    sec_now=ros::Time::now().toSec();
         }  
         if(sqrt(pow(phini-phi,2))>(PI/180)){
             alpha=2;
             phini=phi;
         }
      //  std::cout<<"angulo de rotacion: "<<(double) ((phi*180)/PI)<<std::endl;           
		ros::spinOnce();
		loop_rate.sleep();
			
	}   
	return 0;
}
