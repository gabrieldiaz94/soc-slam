#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>

#define PI 3.14159265
struct Puntos{
 double  x, y,z;
 int n,val,cuadranteC,cuadranteE;
};
struct Voxel{
 int valores [10000][4];
 int valores1 [10000][4];
 int cantidad;
}Cubo;

Puntos* limites(Puntos* punto,int n,int maxX,int minX,int maxY, int minY, int maxZ, int minZ){
    Puntos* limits=new Puntos[n];
    int ii=0;
    for (int i=0;i<n;i++){
        if((punto[i].x>minX && punto[i].x<=maxX) && (punto[i].y>minY && punto[i].y<=maxY) && (punto[i].z>minZ && punto[i].z<=maxZ)){
            limits[ii].x=punto[i].x;
            limits[ii].y=punto[i].y;
            limits[ii].z=punto[i].z;
            limits[ii].val=0;
            ii=ii+1;
        }
    }
    limits[0].n=ii;
    return limits;
}

/*Puntos* organizar(Puntos* puntos, int n,double x, double y, double z){
    int varx=0,varx1=0,varx2=0,vary=0,vary1=0,vary2=0,varz=0,aux;
    double intverval=0.75;
    double eucl[9][4];
    int xx,yy,zz;
    float xx1,yy1,zz1,xx2,yy2,zz2,xx3,yy3,zz3,xx4,yy4,zz4;
    int xcuad=100*x,ycuad=100*y,zcuad=100*z;
    int varuax,va=0;
    int minimos;
    for(int i=0;i<n;i++){
       xx=puntos[i].x*100;
       yy=puntos[i].y*100;
       zz=puntos[i].z*100;
        varx=xx/xcuad;
        varx=varx*xcuad;
        varx1=varx+xcuad;
        varx2=varx-xcuad;
        vary=yy/ycuad;
        vary=vary*ycuad;
        vary1=vary+ycuad;
        vary2=vary-ycuad;
        varz=zz/zcuad;
        varz=varz*zcuad;
        eucl[0][0]=sqrt(pow(xx-varx,2)+pow(yy-vary,2)+pow(zz-varz,2));
        eucl[0][1]=varx;
        eucl[0][2]=vary;
        eucl[0][3]=varz;
        eucl[1][0]=sqrt(pow(xx-varx1,2)+pow(yy-vary,2)+pow(zz-varz,2));
        eucl[1][1]=varx1;
        eucl[1][2]=vary;
        eucl[1][3]=varz;
        eucl[2][0]=sqrt(pow(xx-varx2,2)+pow(yy-vary,2)+pow(zz-varz,2));
        eucl[2][1]=varx2;
        eucl[2][2]=vary;
        eucl[2][3]=varz;
        eucl[3][0]=sqrt(pow(xx-varx,2)+pow(yy-vary1,2)+pow(zz-varz,2));
        eucl[3][1]=varx;
        eucl[3][2]=vary1;
        eucl[3][3]=varz;
        eucl[4][0]=sqrt(pow(xx-varx,2)+pow(yy-vary2,2)+pow(zz-varz,2));
        eucl[4][1]=varx;
        eucl[4][2]=vary2;
        eucl[4][3]=varz;
        eucl[5][0]=sqrt(pow(xx-varx1,2)+pow(yy-vary1,2)+pow(zz-varz,2));
        eucl[5][1]=varx1;
        eucl[5][2]=vary1;
        eucl[5][3]=varz;
        eucl[6][0]=sqrt(pow(xx-varx2,2)+pow(yy-vary1,2)+pow(zz-varz,2));
        eucl[6][1]=varx2;
        eucl[6][2]=vary1;
        eucl[6][3]=varz;
        eucl[7][0]=sqrt(pow(xx-varx1,2)+pow(yy-vary2,2)+pow(zz-varz,2));
        eucl[7][1]=varx1;
        eucl[7][2]=vary2;
        eucl[7][3]=varz;
        eucl[8][0]=sqrt(pow(xx-varx2,2)+pow(yy-vary2,2)+pow(zz-varz,2));
        eucl[8][1]=varx2;
        eucl[8][2]=vary2;
        eucl[8][3]=varz;
        minimos=100000;
        for(int i=0;i<9;i++){
            if(minimos>eucl[i][0]){
                minimos=eucl[i][0];
                varx=eucl[i][1];
                vary=eucl[i][2];
                varz=eucl[i][3];
            }
        }
        if(Cubo.cantidad!=0){
            aux=0;
            for(int j=0;j<Cubo.cantidad;j++){
                if(varx==Cubo.valores[j][0] && vary==Cubo.valores[j][1] && varz==Cubo.valores[j][2]){
                    aux=1;
                    Cubo.valores[j][3]=Cubo.valores[j][3]+1;
                    j=Cubo.cantidad;
                }
            }
            if(aux==0){
                Cubo.valores[Cubo.cantidad][0]=varx;
                Cubo.valores[Cubo.cantidad][1]=vary;
                Cubo.valores[Cubo.cantidad][2]=varz;
                Cubo.valores[Cubo.cantidad][3]=1;
                Cubo.valores1[Cubo.cantidad][0]=xx;
                Cubo.valores1[Cubo.cantidad][1]=yy;
                Cubo.valores1[Cubo.cantidad][2]=zz;
                Cubo.valores1[Cubo.cantidad][3]=i;    
                Cubo.cantidad=Cubo.cantidad+1;    
            }
        }
        if(Cubo.cantidad==0){
            Cubo.valores[Cubo.cantidad][0]=varx;
            Cubo.valores[Cubo.cantidad][1]=vary;
            Cubo.valores[Cubo.cantidad][2]=varz;
            Cubo.valores[Cubo.cantidad][3]=1;
            Cubo.valores1[Cubo.cantidad][0]=xx1/100;
            Cubo.valores1[Cubo.cantidad][1]=puntos[i].y;
            Cubo.valores1[Cubo.cantidad][2]=puntos[i].z;
            Cubo.valores1[Cubo.cantidad][3]=i;
            Cubo.cantidad=Cubo.cantidad+1;
        }
    }
    Puntos* puntosV=new Puntos[Cubo.cantidad];
    int iis=0;
    for(int i=0;i<Cubo.cantidad;i++){
             puntosV[i].x=(double) Cubo.valores[i][0]/100;
             puntosV[i].y= (double) Cubo.valores[i][1]/100;
             puntosV[i].z= (double) Cubo.valores[i][2]/100;
    }
    puntosV[0].n=Cubo.cantidad;
    return  puntosV;
}*/
/*Puntos* organizar(Puntos* puntos, int n,double x, double y, double z){
    int varx=0,varx1=0,varx2=0,vary=0,vary1=0,vary2=0,varz=0,aux;
    double intverval=0.75;
    double eucl[9][4];
    int xx,yy,zz,X[n][3],Y[n][3],Z[n][3],cantx=0,canty=0,cantz=0;
    Voxel Cubolocal;
    Cubolocal.cantidad=0;
    float xx1,yy1,zz1,xx2,yy2,zz2,xx3,yy3,zz3,xx4,yy4,zz4;
    int xcuad=100*x,ycuad=100*y,zcuad=100*z;
    int varuax,va=0;
    int minimos;
    for(int i=0;i<n;i++){
       xx=puntos[i].x*100;
       yy=puntos[i].y*100;
       zz=puntos[i].z*100;
        varx=xx/xcuad;
        varx=varx*xcuad;
        varx1=varx+xcuad;
        varx2=varx-xcuad;
        vary=yy/ycuad;
        vary=vary*ycuad;
        vary1=vary+ycuad;
        vary2=vary-ycuad;
        varz=zz/zcuad;
        varz=varz*zcuad;
        eucl[0][0]=sqrt(pow(xx-varx,2)+pow(yy-vary,2)+pow(zz-varz,2));
        eucl[0][1]=varx;
        eucl[0][2]=vary;
        eucl[0][3]=varz;
        eucl[1][0]=sqrt(pow(xx-varx1,2)+pow(yy-vary,2)+pow(zz-varz,2));
        eucl[1][1]=varx1;
        eucl[1][2]=vary;
        eucl[1][3]=varz;
        eucl[2][0]=sqrt(pow(xx-varx2,2)+pow(yy-vary,2)+pow(zz-varz,2));
        eucl[2][1]=varx2;
        eucl[2][2]=vary;
        eucl[2][3]=varz;
        eucl[3][0]=sqrt(pow(xx-varx,2)+pow(yy-vary1,2)+pow(zz-varz,2));
        eucl[3][1]=varx;
        eucl[3][2]=vary1;
        eucl[3][3]=varz;
        eucl[4][0]=sqrt(pow(xx-varx,2)+pow(yy-vary2,2)+pow(zz-varz,2));
        eucl[4][1]=varx;
        eucl[4][2]=vary2;
        eucl[4][3]=varz;
        eucl[5][0]=sqrt(pow(xx-varx1,2)+pow(yy-vary1,2)+pow(zz-varz,2));
        eucl[5][1]=varx1;
        eucl[5][2]=vary1;
        eucl[5][3]=varz;
        eucl[6][0]=sqrt(pow(xx-varx2,2)+pow(yy-vary1,2)+pow(zz-varz,2));
        eucl[6][1]=varx2;
        eucl[6][2]=vary1;
        eucl[6][3]=varz;
        eucl[7][0]=sqrt(pow(xx-varx1,2)+pow(yy-vary2,2)+pow(zz-varz,2));
        eucl[7][1]=varx1;
        eucl[7][2]=vary2;
        eucl[7][3]=varz;
        eucl[8][0]=sqrt(pow(xx-varx2,2)+pow(yy-vary2,2)+pow(zz-varz,2));
        eucl[8][1]=varx2;
        eucl[8][2]=vary2;
        eucl[8][3]=varz;
        minimos=100000;
        for(int i=0;i<9;i++){
            if(minimos>eucl[i][0]){
                minimos=eucl[i][0];
                varx=eucl[i][1];
                vary=eucl[i][2];
                varz=eucl[i][3];
            }
        }
        if(Cubolocal.cantidad!=0){
            aux=0;
            for(int j=0;j<Cubolocal.cantidad;j++){
                if(varx==Cubolocal.valores[j][0] && vary==Cubolocal.valores[j][1] && varz==Cubolocal.valores[j][2]){
                    aux=1;
                    Cubolocal.valores[j][3]=Cubolocal.valores[j][3]+1;
                    j=Cubolocal.cantidad;
                }
            }
            if(aux==0){
                Cubolocal.valores[Cubolocal.cantidad][0]=varx;
                Cubolocal.valores[Cubolocal.cantidad][1]=vary;
                Cubolocal.valores[Cubolocal.cantidad][2]=varz;
                Cubolocal.valores[Cubolocal.cantidad][3]=1;
                Cubolocal.valores1[Cubolocal.cantidad][0]=xx;
                Cubolocal.valores1[Cubolocal.cantidad][1]=yy;
                Cubolocal.valores1[Cubolocal.cantidad][2]=zz;
                Cubolocal.valores1[Cubolocal.cantidad][3]=i;    
                Cubolocal.cantidad=Cubolocal.cantidad+1;    
            }
        }
        if(Cubolocal.cantidad==0){
            Cubolocal.valores[Cubolocal.cantidad][0]=varx;
            Cubolocal.valores[Cubolocal.cantidad][1]=vary;
            Cubolocal.valores[Cubolocal.cantidad][2]=varz;
            Cubolocal.valores[Cubolocal.cantidad][3]=1;
            Cubolocal.valores1[Cubolocal.cantidad][0]=xx;
            Cubolocal.valores1[Cubolocal.cantidad][1]=yy;
            Cubolocal.valores1[Cubolocal.cantidad][2]=zz;
            Cubolocal.valores1[Cubolocal.cantidad][3]=i;
            Cubolocal.cantidad=Cubolocal.cantidad+1;
        }
        if(cantx==0){
            X[cantx][0]=varx;
            X[cantx][1]=0;
            X[cantx][2]=xx;
            cantx=cantx+1;
        }else{
            va=0;
            for(int j=0;j<cantx;j++){
                if(varx==X[j][0]){
                    X[j][1]=X[j][1]+1;
                    va=1;
                }
            }
            if(va==0){
                X[cantx][0]=varx;
                X[cantx][1]=0;
                X[cantx][2]=xx;
                cantx=cantx+1;
            }
        }
        
        if(canty==0){
            Y[canty][0]=vary;
            Y[canty][1]=0;
            Y[canty][2]=yy;
            canty=canty+1;
        }else{
            va=0;
            for(int j=0;j<canty;j++){
                if(vary==Y[j][0]){
                    Y[j][1]=Y[j][1]+1;
                    va=1;
                }
            }
            if(va==0){
                Y[canty][0]=vary;
                Y[canty][1]=0;
                Y[canty][2]=yy;
                canty=canty+1;
            }
        }
    }
    std::cout<<"Cantidad de puntos en X: "<<cantx<<std::endl;
    std::cout<<"Cantidad de puntos en Y: "<<canty<<std::endl;
    int mayoresy[3] {};
    int mayoresym[3] {};
    for(int i=0;i<canty;i++){
        if(Y[i][0]>=0){
            if(mayoresy[0]<Y[i][1]){
                mayoresy[0]=Y[i][1];
                mayoresy[1]=Y[i][0];
                mayoresy[2]=Y[i][2];
            }
        }
        if(Y[i][0]<0){
            if(mayoresym[0]<Y[i][1]){
                mayoresym[0]=Y[i][1];
                mayoresym[1]=Y[i][0];
                mayoresym[2]=Y[i][2];
            }
        }
    }
    int mayoresx[3]{};
    int mayoresxm[3]{};
    for(int i=0;i<cantx;i++){
        if(X[i][0]>=0){
            if(mayoresx[0]<X[i][1]){
                mayoresx[0]=X[i][1];
                mayoresx[1]=X[i][0];
                mayoresx[2]=X[i][2];
            }
        }
        if(X[i][0]<0){
            if(mayoresxm[0]<X[i][1]){
                mayoresxm[0]=X[i][1];
                mayoresxm[1]=X[i][0];
                mayoresxm[2]=X[i][2];
            }
        }
    }
    int cuantos=0,aux1=0;
    aux=0;
    for(int i=0;i<Cubolocal.cantidad;i++){
        aux=0;
        if(mayoresy[1]==Cubolocal.valores[i][1] || mayoresy[1]-ycuad==Cubolocal.valores[i][1] || mayoresy[1]+ycuad==Cubolocal.valores[i][1]){
            cuantos=sqrt(pow(Cubolocal.valores1[i][1]-mayoresy[2],2));
            if(cuantos<5){
                    Cubolocal.valores[i][1]=mayoresy[1];
            }
        }
        if(mayoresym[1]+ycuad==Cubolocal.valores[i][1] || mayoresym[1]-ycuad==Cubolocal.valores[i][1] || mayoresym[1]==Cubolocal.valores[i][1]){
            cuantos=sqrt(pow(Cubolocal.valores1[i][1]-mayoresym[2],2));
            if(cuantos<5){
                    Cubolocal.valores[i][1]=mayoresym[1];
            }
        }
        
        if(mayoresx[1]==Cubolocal.valores[i][0] || mayoresx[1]-xcuad==Cubolocal.valores[i][0] || mayoresx[1]+xcuad==Cubolocal.valores[i][0]){
            cuantos=sqrt(pow(Cubolocal.valores1[i][0]-mayoresx[2],2));
            if(cuantos<5){
                    Cubolocal.valores[i][0]=mayoresx[1];
            }
        }

        if(mayoresxm[1]+xcuad==Cubolocal.valores[i][0] || mayoresxm[1]-xcuad==Cubolocal.valores[i][0] || mayoresxm[1]==Cubolocal.valores[i][0]){
            cuantos=sqrt(pow(Cubolocal.valores1[i][0]-mayoresxm[2],2));
            if(cuantos<5){
                Cubolocal.valores[i][0]=mayoresxm[1];
            }
        }
    }
    Puntos* puntosV=new Puntos[Cubolocal.cantidad];
    int iis=0;
    for(int i=0;i<Cubolocal.cantidad;i++){
           //  if(mayoresy[1]==Cubolocal.valores[i][1]  || mayoresym[1]==Cubolocal.valores[i][1]){
                    puntosV[iis].x=(double) Cubolocal.valores[i][0]/100;
                    puntosV[iis].y= (double) Cubolocal.valores[i][1]/100;
                    puntosV[iis].z= (double) Cubolocal.valores[i][2]/100;
                    iis=iis+1;
          //   }
    }
    puntosV[0].n=iis;
    return  puntosV;
}*/

/*Puntos* organizar(Puntos* puntos, int n,double x, double y, double z){
    int varx=0,vary=0,varz=0,aux;
    int xx,yy,zz,cantx=0,canty=0,cantz=0;
    float coordrad=0,coordrad1=0,coordrad2=0,coordrad3=0,rad1=0;
    float coordreg=10,coordreg1=10,coordreg2=10,coordreg3=10,reg=0,reg1=0;
    int puntos_coord[n][3],puntos_coord1[n][3],puntos_coord2[n][3],puntos_coord3[n][3],contar=0,contar1=0,contar2=0,contar3=0;
    Voxel Cubolocal;
    double coordregor;
    Cubolocal.cantidad=0;
    int xcuad=100*x,ycuad=100*y,zcuad=100*z;   
    for(int i=0;i<n;i++){
       xx=puntos[i].x*100;
       yy=puntos[i].y*100;
       zz=puntos[i].z*100;
       rad1=sqrt(pow(xx,2)+pow(yy,2));
       if(xx<0 && yy<0){
           reg=-1*(xx/rad1);
           reg=acos(reg)*180.0/PI;
           reg1=-1*(yy/rad1);
           reg1=asin(reg1)*180.0/PI;
           if(rad1>coordrad){
               coordrad=rad1;
               coordreg=reg;
           }
           puntos_coord[contar][0]=xx;
           puntos_coord[contar][1]=yy;
           puntos_coord[contar][2]=zz;
           contar=contar+1;
       }
       if(xx<0 && yy>=0){
           reg1=yy/rad1;
           reg1=asin(reg1)*180.0/PI;
           if(rad1>coordrad1){
               coordrad1=rad1;
               coordreg1=reg1;
               coordregor=atan2(yy,xx)*180.0/PI;
           }
           puntos_coord1[contar1][0]=xx;
           puntos_coord1[contar1][1]=yy;
           puntos_coord1[contar1][2]=zz;
           contar1=contar1+1;
       }
       if(xx>=0 && yy<0){
           reg=xx/rad1;
           reg=acos(reg)*180.0/PI;
           if(rad1>coordrad){
               coordrad2=rad1;
               coordreg2=reg;
           }
           puntos_coord2[contar2][0]=xx;
           puntos_coord2[contar2][1]=yy;
           puntos_coord2[contar2][2]=zz;
           contar2=contar2+1;
       }
       if(xx>=0 && yy>=0){
           reg=xx/rad1;
           reg=acos(reg)*180.0/PI;
           if(rad1>coordrad){
               coordrad3=rad1;
               coordreg3=reg;
           }
           puntos_coord3[contar3][0]=xx;
           puntos_coord3[contar3][1]=yy;
           puntos_coord3[contar3][2]=zz;
           contar3=contar3+1;
       }
    }
    int contar01=0,contar02=0,contar11=0,contar12=0,contar21=0,contar22=0,contar31=0,contar32=0;
    int coord01[contar][3],coord02[contar][3],coord11[contar1][3],coord12[contar1][3],coord21[contar2][3],coord22[contar2][3],coord31[contar3][3],coord32[contar3][3];

    for(int i=0;i<contar;i++){
        rad1=sqrt(pow(puntos_coord[i][0],2)+pow(puntos_coord[i][1],2));
        reg=-1*(puntos_coord[i][0]/rad1);
        reg=acos(reg)*180/PI;
        if(reg>=coordreg){
            coord01[contar01][0]=puntos_coord[i][0];
            coord01[contar01][1]=puntos_coord[i][1];
            coord01[contar01][2]=puntos_coord[i][2];
            contar01=contar01+1;
        }else{
            coord02[contar02][0]=puntos_coord[i][0];
            coord02[contar02][1]=puntos_coord[i][1];
            coord02[contar02][2]=puntos_coord[i][2];
            contar02=contar02+1;
        }
    }
    for(int i=0;i<contar1;i++){
        rad1=sqrt(pow(puntos_coord1[i][0],2)+pow(puntos_coord1[i][1],2));
        reg=(puntos_coord1[i][1]/rad1);
        reg=asin(reg)*180/PI;
        if(reg>=coordreg1){
            coord11[contar11][0]=puntos_coord1[i][0];
            coord11[contar11][1]=puntos_coord1[i][1];
            coord11[contar11][2]=puntos_coord1[i][2];
            contar11=contar11+1;
        }else{
            coord12[contar12][0]=puntos_coord1[i][0];
            coord12[contar12][1]=puntos_coord1[i][1];
            coord12[contar12][2]=puntos_coord1[i][2];
            contar12=contar12+1;
        }
    }

    for(int i=0;i<contar2;i++){
        rad1=sqrt(pow(puntos_coord2[i][0],2)+pow(puntos_coord2[i][1],2));
        reg=(puntos_coord2[i][0]/rad1);
        reg=acos(reg)*180/PI;
        if(reg>=coordreg2){
            coord21[contar21][0]=puntos_coord2[i][0];
            coord21[contar21][1]=puntos_coord2[i][1];
            coord21[contar21][2]=puntos_coord2[i][2];
            contar21=contar21+1;
        }else{
            coord22[contar22][0]=puntos_coord2[i][0];
            coord22[contar22][1]=puntos_coord2[i][1];
            coord22[contar22][2]=puntos_coord2[i][2];
            contar22=contar22+1;
        }
    }

    for(int i=0;i<contar3;i++){
        rad1=sqrt(pow(puntos_coord3[i][0],2)+pow(puntos_coord3[i][1],2));
        reg=(puntos_coord3[i][0]/rad1);
        reg=acos(reg)*180/PI;
        if(reg>=coordreg3){
            coord31[contar31][0]=puntos_coord3[i][0];
            coord31[contar31][1]=puntos_coord3[i][1];
            coord31[contar31][2]=puntos_coord3[i][2];
            contar31=contar31+1;
        }else{
            coord32[contar32][0]=puntos_coord3[i][0];
            coord32[contar32][1]=puntos_coord3[i][1];
            coord32[contar32][2]=puntos_coord3[i][2];
            contar32=contar32+1;
        }
    }
    int frente1[contar02+contar12][3],frente2[contar11+contar31][3],frente3[contar22+contar32][3],frente4[contar01+contar21][3];
    int cfrente1=0,cfrente2=0,cfrente3=0,cfrente4=0;
    for(int i=0;i<contar02+contar12;i++){
        if(i<contar02){
            frente1[cfrente1][0]=coord02[i][0];
            frente1[cfrente1][1]=coord02[i][1];
            frente1[cfrente1][2]=coord02[i][2];
            cfrente1=cfrente1+1;
        }
        if( i-contar02>0 && i-contar02<contar12){
            frente1[cfrente1][0]=coord12[i-contar02][0];
            frente1[cfrente1][1]=coord12[i-contar02][1];
            frente1[cfrente1][2]=coord12[i-contar02][2];
            cfrente1=cfrente1+1;

        }
    }
    //Primer Cuadrante
    int nuevofrente1[cfrente1][3],ncfrente1=0,Xf1[cfrente1][2],aux1=0;
    double theta,radius,radius1,theta1;
    int xvari,Xres[cfrente1][3],Xres1[cfrente1];
    int contrad=0,contares=0,contaresT=0,contares1=0;
    int vary1,varz1;
    for(int i=0;i<cfrente1;i++){         
        vary=frente1[i][1]/ycuad;
        vary=vary*ycuad;
        varz=frente1[i][2]/zcuad;
        varz=varz*zcuad;
        if(ncfrente1>0){
            aux=0;
            for(int j=0;j<ncfrente1;j++){
                if(nuevofrente1[j][2]==varz && nuevofrente1[j][0]==frente1[i][0] && nuevofrente1[j][1]==frente1[i][1]){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente1[ncfrente1][0]=frente1[i][0];
                nuevofrente1[ncfrente1][1]=frente1[i][1];
                nuevofrente1[ncfrente1][2]=varz;
                ncfrente1=ncfrente1+1;
            }
        }else{
                nuevofrente1[ncfrente1][0]=frente1[i][0];
                nuevofrente1[ncfrente1][1]=frente1[i][1];
                nuevofrente1[ncfrente1][2]=varz;
                ncfrente1=ncfrente1+1;
            }
    }
    // Analisis cuadrante x
    radius1=0;
    aux=0;
    contares=0;
    int minimo=1000,maximo=-1000;
    contares1=0;
    contaresT=0;
    for(int i=0;i<ncfrente1;i++){
        aux=0;
        minimo=1000;
        maximo=-1000;
        theta1=atan2(nuevofrente1[i][1],nuevofrente1[i][0])*180.0/PI;
        radius=sqrt(pow(xcuad,2)+pow(ycuad,2));
        if(i==0){
            contaresT=contares;
            Xres[contares][0]=i;
            contares=contares+1;
        }
        aux=0;
        if(i>0){
            contaresT=contares;
            for(int j=0;j<contares;j++){
                if(i==Xres[j][0]){
                    aux=1;
                }
            }
        }
        if(aux==0){
            Xres[contares][0]=i;
            contares=contares+1;
            minimo=nuevofrente1[i][0];
            maximo=nuevofrente1[i][0];
            for(int j=0;j<ncfrente1;j++){
                if(j!=i){
                    radius1=sqrt(pow(nuevofrente1[i][0]-nuevofrente1[j][0],2)+pow(nuevofrente1[i][1]-nuevofrente1[j][1],2));
                    theta=atan2(nuevofrente1[j][1],nuevofrente1[j][0])*180.0/PI;
                // difftheta=sqrt(pow(theta-theta1,2));
                    if(radius1<=radius){
                        if(i==0){
                            Xres[contares][0]=j;
                            contares=contares+1;
                            if(minimo>nuevofrente1[j][0]){
                                minimo=nuevofrente1[j][0];
                            }
                            if(maximo<nuevofrente1[j][0]){
                                maximo=nuevofrente1[j][0];
                            }
                        }
                        if(i>0){
                            aux1=0;
                            for(int k=0;k<contares;k++){
                                if(j==Xres[k][0]){
                                    aux1=1;    
                                }    
                            }
                            if(aux1==0){
                                Xres[contares][0]=j;
                                contares=contares+1;
                                if(minimo>nuevofrente1[j][0]){
                                    minimo=nuevofrente1[j][0];
                                }
                                if(maximo<nuevofrente1[j][0]){
                                    maximo=nuevofrente1[j][0];
                                }
                            }   
                        }            
                    }
                }
            }

            Xres1[contares1]=contares;
            contares1=contares1+1;
            for(int j=contaresT;j<contares;j++){
                Xres[j][1]=minimo;
                Xres[j][2]=maximo;
            }
        }   

    }
    // segunda parte
    aux=0;
    aux1=0;
    int xx0,xx02,xx01;
    for(int i=0;i<contares1;i++){
            aux=1;
            aux=0;
                for(int j=0;j<contares1;j++){
                    if(j!=i){
                        xx0=sqrt(pow(Xres[Xres1[i]-1][1],2));
                        xx01=sqrt(pow(Xres[Xres1[j]-1][1],2));
                        if(xx0>xx01){
                            xx0=sqrt(pow(Xres[Xres1[i]-1][2],2));
                            aux1=1;   
                        }
                        if(xx0<xx01){
                            xx01=sqrt(pow(Xres[Xres1[j]-1][2],2));
                            aux1=2;   
                        }
                        if(xx0==xx01){
                            aux1=0;
                        }
                        if(aux1==1 || aux1==2){
                            xx02=sqrt(pow(xx0-xx01,2));
                            if(xx02<=10){
                                if(aux1==2){
                                    Xres[Xres1[i]-1][1]=Xres[Xres1[j]-1][1];
                                    Xres[Xres1[i]-1][2]=Xres[Xres1[j]-1][2];
                                }
                                aux=1;   
                            }
                        }                      
                    }
                }     
    }
    //tercera Parte
    aux1=0;
    for(int i=0;i<contares;i++){
        if(i==Xres1[aux1]){
            if(aux1<contares1){
                aux1=aux1+1;
            }
        }
        if(i<Xres1[aux1]){
            xx0=Xres[Xres1[aux1]-1][1];
            xx01=Xres[Xres1[aux1]-1][2];
            Xres[i][1]=xx0;
            Xres[i][2]=xx01;
        }
    }
    //Parte final
    int ncfrente01=0;
    int nuevofrente01[cfrente1][3];    
    for(int i=0; i<ncfrente1;i++){
        vary=nuevofrente1[i][1]/ycuad;
        vary=vary*ycuad;
        varz=nuevofrente1[i][2]/zcuad;
        varz=varz*zcuad;
        for(int j=0;j<contares;j++){
            if(i==Xres[j][0]){
                varx=Xres[j][1];
                varx=varx/xcuad;
                varx=varx*xcuad;
            }
        }
        if(ncfrente01>0){
            aux=0;
            for(int j=0;j<ncfrente01;j++){
                if(nuevofrente01[j][0]==varx && nuevofrente01[j][1]==vary && nuevofrente01[j][2]==varz){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente01[ncfrente01][0]=varx;
                nuevofrente01[ncfrente01][1]=vary;
                nuevofrente01[ncfrente01][2]=varz;
                ncfrente01=ncfrente01+1;
            }
        }
        if(ncfrente01==0){
            nuevofrente01[ncfrente01][0]=varx;
            nuevofrente01[ncfrente01][1]=vary;
            nuevofrente01[ncfrente01][2]=varz;
            ncfrente01=ncfrente01+1;
        }
    }


    //Segundo Cuadrante 
    for(int i=0;i<contar11+contar31;i++){
        if(i<contar11){
            frente2[cfrente2][0]=coord11[i][0];
            frente2[cfrente2][1]=coord11[i][1];
            frente2[cfrente2][2]=coord11[i][2];
            cfrente2=cfrente2+1;
        }
        if( i-contar11>0 && i-contar11<contar31){
            frente2[cfrente2][0]=coord31[i-contar11][0];
            frente2[cfrente2][1]=coord31[i-contar11][1];
            frente2[cfrente2][2]=coord31[i-contar11][2];
            cfrente2=cfrente2+1;

        }
    }

    int nuevofrente2[cfrente2][3],ncfrente2=0;
    for(int i=0;i<cfrente2;i++){
        varz=frente2[i][2]/zcuad;
        varz=varz*zcuad;
        if(ncfrente2>0){
            aux=0;
            for(int j=0;j<ncfrente2;j++){
                if(nuevofrente2[j][0]==nuevofrente2[i][0] && nuevofrente2[j][1]==nuevofrente2[i][1] && nuevofrente2[j][2]==varz){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente2[ncfrente2][0]=frente2[i][0];
                nuevofrente2[ncfrente2][1]=frente2[i][1];
                nuevofrente2[ncfrente2][2]=varz;
                ncfrente2=ncfrente2+1;    
            }
        }
        if(ncfrente2==0){
            nuevofrente2[ncfrente2][0]=varx;
            nuevofrente2[ncfrente2][1]=vary;
            nuevofrente2[ncfrente2][2]=varz;
            ncfrente2=ncfrente2+1;
        }
    }

// Analisis cuadrante x
    radius1=0;
    aux=0;
    contares=0;
    minimo=1000,maximo=-1000;
    contares1=0;
    contaresT=0;
    for(int i=0;i<ncfrente2;i++){
        aux=0;
        minimo=1000;
        maximo=-1000;
        theta1=atan2(nuevofrente2[i][1],nuevofrente2[i][0])*180.0/PI;
        radius=sqrt(pow(xcuad,2)+pow(ycuad,2));
        if(i==0){
            contaresT=contares;
            Xres[contares][0]=i;
            contares=contares+1;
        }
        aux=0;
        if(i>0){
            contaresT=contares;
            for(int j=0;j<contares;j++){
                if(i==Xres[j][0]){
                    aux=1;
                }
            }
        }
        if(aux==0){
            Xres[contares][0]=i;
            contares=contares+1;
            minimo=nuevofrente2[i][1];
            maximo=nuevofrente2[i][1];
            for(int j=0;j<ncfrente2;j++){
                if(j!=i){
                    radius1=sqrt(pow(nuevofrente2[i][0]-nuevofrente2[j][0],2)+pow(nuevofrente2[i][1]-nuevofrente2[j][1],2));
                    theta=atan2(nuevofrente2[j][1],nuevofrente2[j][0])*180.0/PI;
                // difftheta=sqrt(pow(theta-theta1,2));
                    if(radius1<=radius){
                        if(i==0){
                            Xres[contares][0]=j;
                            contares=contares+1;
                            if(minimo>nuevofrente2[j][1]){
                                minimo=nuevofrente2[j][1];
                            }
                            if(maximo<nuevofrente2[j][1]){
                                maximo=nuevofrente2[j][1];
                            }
                        }
                        if(i>0){
                            aux1=0;
                            for(int k=0;k<contares;k++){
                                if(j==Xres[k][0]){
                                    aux1=1;    
                                }    
                            }
                            if(aux1==0){
                                Xres[contares][0]=j;
                                contares=contares+1;
                                if(minimo>nuevofrente2[j][1]){
                                    minimo=nuevofrente2[j][1];
                                }
                                if(maximo<nuevofrente2[j][1]){
                                    maximo=nuevofrente2[j][1];
                                }
                            }   
                        }            
                    }
                }
            }

            Xres1[contares1]=contares;
            contares1=contares1+1;
            for(int j=contaresT;j<contares;j++){
                Xres[j][1]=maximo;
                Xres[j][2]=minimo;
            }
        }   

    }
    // segunda parte
    aux=0;
    aux1=0;
    for(int i=0;i<contares1;i++){
            aux=1;
            aux=0;
                for(int j=0;j<contares1;j++){
                    if(j!=i){
                        xx0=sqrt(pow(Xres[Xres1[i]-1][1],2));
                        xx01=sqrt(pow(Xres[Xres1[j]-1][1],2));
                        if(xx0>xx01){
                            xx0=sqrt(pow(Xres[Xres1[i]-1][2],2));
                            aux1=1;   
                        }
                        if(xx0<xx01){
                            xx01=sqrt(pow(Xres[Xres1[j]-1][2],2));
                            aux1=2;   
                        }
                        if(xx0==xx01){
                            aux1=0;
                        }
                        if(aux1==1 || aux1==2){
                            xx02=sqrt(pow(xx0-xx01,2));
                            if(xx02<=10){
                                if(aux1==2){
                                    Xres[Xres1[i]-1][1]=Xres[Xres1[j]-1][1];
                                    Xres[Xres1[i]-1][2]=Xres[Xres1[j]-1][2];
                                }
                                aux=1;   
                            }
                        }                      
                    }
                }     
    }

//tercera Parte
    aux1=0;
    for(int i=0;i<contares;i++){
        if(i==Xres1[aux1]){
            if(aux1<contares1){
                aux1=aux1+1;
            }
        }
        if(i<Xres1[aux1]){
            xx0=Xres[Xres1[aux1]-1][1];
            xx01=Xres[Xres1[aux1]-1][2];
            Xres[i][1]=xx0;
            Xres[i][2]=xx01;
        }
    }


//Parte final
    int ncfrente02=0;
    int nuevofrente02[cfrente2][3];    
    for(int i=0; i<ncfrente2;i++){
        varx=nuevofrente2[i][0]/xcuad;
        varx=varx*xcuad;
        varz=nuevofrente2[i][2]/zcuad;
        varz=varz*zcuad;
        for(int j=0;j<contares;j++){
            if(i==Xres[j][0]){
                vary=Xres[j][1];
                vary=vary/ycuad;
                vary=vary*ycuad;
            }
        }
        if(ncfrente02>0){
            aux=0;
            for(int j=0;j<ncfrente02;j++){
                if(nuevofrente02[j][0]==varx && nuevofrente02[j][1]==vary && nuevofrente02[j][2]==varz){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente02[ncfrente02][0]=varx;
                nuevofrente02[ncfrente02][1]=vary;
                nuevofrente02[ncfrente02][2]=varz;
                ncfrente02=ncfrente02+1;
            }
        }
        if(ncfrente02==0){
            nuevofrente02[ncfrente02][0]=varx;
            nuevofrente02[ncfrente02][1]=vary;
            nuevofrente02[ncfrente02][2]=varz;
            ncfrente02=ncfrente02+1;
        }
    }
// Cuadrante 3
    for(int i=0;i<contar22+contar32;i++){
        if(i<contar22){
            frente3[cfrente3][0]=coord22[i][0];
            frente3[cfrente3][1]=coord22[i][1];
            frente3[cfrente3][2]=coord22[i][2];
            cfrente3=cfrente3+1;
        }
        if( i-contar22>0 && i-contar22<contar32){
            frente3[cfrente3][0]=coord32[i-contar22][0];
            frente3[cfrente3][1]=coord32[i-contar22][1];
            frente3[cfrente3][2]=coord32[i-contar22][2];
            cfrente3=cfrente3+1;

        }
    }
  int nuevofrente3[cfrente3][3],ncfrente3=0;
    for(int i=0;i<cfrente3;i++){
        varz=frente3[i][2]/zcuad;
        varz=varz*zcuad;
        if(ncfrente3>0){
            aux=0;
            for(int j=0;j<ncfrente3;j++){
                if(nuevofrente3[j][0]==nuevofrente3[i][0] && nuevofrente3[j][1]==nuevofrente3[i][1] && nuevofrente3[j][2]==varz){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente3[ncfrente3][0]=frente3[i][0];
                nuevofrente3[ncfrente3][1]=frente3[i][1];
                nuevofrente3[ncfrente3][2]=varz;
                ncfrente3=ncfrente3+1;    
            }
        }
        if(ncfrente3==0){
            nuevofrente3[ncfrente3][0]=frente3[i][0];
            nuevofrente3[ncfrente3][1]=frente3[i][1];
            nuevofrente3[ncfrente3][2]=varz;
            ncfrente3=ncfrente3+1;
        }
    }

// Analisis cuadrante x
    radius1=0;
    aux=0;
    contares=0;
    minimo=1000,maximo=-1000;
    contares1=0;
    contaresT=0;
    for(int i=0;i<ncfrente3;i++){
        aux=0;
        minimo=1000;
        maximo=-1000;
        theta1=atan2(nuevofrente3[i][1],nuevofrente3[i][0])*180.0/PI;
        radius=sqrt(pow(xcuad,2)+pow(ycuad,2));
        if(i==0){
            contaresT=contares;
            Xres[contares][0]=i;
            contares=contares+1;
        }
        aux=0;
        if(i>0){
            contaresT=contares;
            for(int j=0;j<contares;j++){
                if(i==Xres[j][0]){
                    aux=1;
                }
            }
        }
        if(aux==0){
            Xres[contares][0]=i;
            contares=contares+1;
            minimo=nuevofrente3[i][0];
            maximo=nuevofrente3[i][0];
            for(int j=0;j<ncfrente3;j++){
                if(j!=i){
                    radius1=sqrt(pow(nuevofrente3[i][0]-nuevofrente3[j][0],2)+pow(nuevofrente3[i][1]-nuevofrente3[j][1],2));
                    theta=atan2(nuevofrente3[j][1],nuevofrente3[j][0])*180.0/PI;
                // difftheta=sqrt(pow(theta-theta1,2));
                    if(radius1<=radius){
                        if(i==0){
                            Xres[contares][0]=j;
                            contares=contares+1;
                            if(minimo>nuevofrente3[j][0]){
                                minimo=nuevofrente3[j][0];
                            }
                            if(maximo<nuevofrente3[j][0]){
                                maximo=nuevofrente3[j][0];
                            }
                        }
                        if(i>0){
                            aux1=0;
                            for(int k=0;k<contares;k++){
                                if(j==Xres[k][0]){
                                    aux1=1;    
                                }    
                            }
                            if(aux1==0){
                                Xres[contares][0]=j;
                                contares=contares+1;
                                if(minimo>nuevofrente3[j][0]){
                                    minimo=nuevofrente3[j][0];
                                }
                                if(maximo<nuevofrente3[j][0]){
                                    maximo=nuevofrente3[j][0];
                                }
                            }   
                        }            
                    }
                }
            }

            Xres1[contares1]=contares;
            contares1=contares1+1;
            for(int j=contaresT;j<contares;j++){
                Xres[j][1]=maximo;
                Xres[j][2]=minimo;
            }
        }   

    }
// segunda parte
    aux=0;
    aux1=0;
    for(int i=0;i<contares1;i++){
            aux=1;
            aux=0;
                for(int j=0;j<contares1;j++){
                    if(j!=i){
                        xx0=sqrt(pow(Xres[Xres1[i]-1][1],2));
                        xx01=sqrt(pow(Xres[Xres1[j]-1][1],2));
                        if(xx0>xx01){
                            xx0=sqrt(pow(Xres[Xres1[i]-1][2],2));
                            aux1=1;   
                        }
                        if(xx0<xx01){
                            xx01=sqrt(pow(Xres[Xres1[j]-1][2],2));
                            aux1=2;   
                        }
                        if(xx0==xx01){
                            aux1=0;
                        }
                        if(aux1==1 || aux1==2){
                            xx02=sqrt(pow(xx0-xx01,2));
                            if(xx02<=10){
                                if(aux1==2){
                                    Xres[Xres1[i]-1][1]=Xres[Xres1[j]-1][1];
                                    Xres[Xres1[i]-1][2]=Xres[Xres1[j]-1][2];
                                }
                                aux=1;   
                            }
                        }                      
                    }
                }     
    }

    //tercera Parte
    aux1=0;
    for(int i=0;i<contares;i++){
        if(i==Xres1[aux1]){
            if(aux1<contares1){
                aux1=aux1+1;
            }
        }
        if(i<Xres1[aux1]){
            xx0=Xres[Xres1[aux1]-1][1];
            xx01=Xres[Xres1[aux1]-1][2];
            Xres[i][1]=xx0;
            Xres[i][2]=xx01;
        }
    }


//Parte final
    int ncfrente03=0;
    int nuevofrente03[cfrente3][3];    
    for(int i=0; i<ncfrente3;i++){
        vary=nuevofrente3[i][1]/ycuad;
        vary=vary*ycuad;
        varz=nuevofrente3[i][2]/zcuad;
        varz=varz*zcuad;
        for(int j=0;j<contares;j++){
            if(i==Xres[j][0]){
                varx=Xres[j][1];
                varx=varx/xcuad;
                varx=varx*xcuad;
            }
        }
        if(ncfrente03>0){
            aux=0;
            for(int j=0;j<ncfrente03;j++){
                if(nuevofrente03[j][0]==varx && nuevofrente03[j][1]==vary && nuevofrente03[j][2]==varz){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente03[ncfrente03][0]=varx;
                nuevofrente03[ncfrente03][1]=vary;
                nuevofrente03[ncfrente03][2]=varz;
                ncfrente03=ncfrente03+1;
            }
        }
        if(ncfrente03==0){
            nuevofrente03[ncfrente03][0]=varx;
            nuevofrente03[ncfrente03][1]=vary;
            nuevofrente03[ncfrente03][2]=varz;
            ncfrente03=ncfrente03+1;
        }
    }






 //Cuarto Cuadrante   

    for(int i=0;i<contar01+contar21;i++){
        if(i<contar01){
            frente4[cfrente4][0]=coord01[i][0];
            frente4[cfrente4][1]=coord01[i][1];
            frente4[cfrente4][2]=coord01[i][2];
            cfrente4=cfrente4+1;
        }
        if( i-contar01>0 && i-contar01<contar21){
            frente4[cfrente4][0]=coord21[i-contar01][0];
            frente4[cfrente4][1]=coord21[i-contar01][1];
            frente4[cfrente4][2]=coord21[i-contar01][2];
            cfrente4=cfrente4+1;

        }
    }

int nuevofrente4[cfrente4][3],ncfrente4=0;
    for(int i=0;i<cfrente4;i++){
        varz=frente4[i][2]/zcuad;
        varz=varz*zcuad;
        if(ncfrente4>0){
            aux=0;
            for(int j=0;j<ncfrente4;j++){
                if(nuevofrente4[j][0]==nuevofrente4[i][0] && nuevofrente4[j][1]==nuevofrente4[i][1] && nuevofrente4[j][2]==varz){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente4[ncfrente4][0]=frente4[i][0];
                nuevofrente4[ncfrente4][1]=frente4[i][1];
                nuevofrente4[ncfrente4][2]=varz;
                ncfrente4=ncfrente4+1;    
            }
        }
        if(ncfrente4==0){
            nuevofrente4[ncfrente4][0]=frente4[i][0];
            nuevofrente4[ncfrente4][1]=frente4[i][1];
            nuevofrente4[ncfrente4][2]=varz;
            ncfrente4=ncfrente4+1;
        }
    }

// Analisis cuadrante x
    radius1=0;
    aux=0;
    contares=0;
    minimo=1000,maximo=-1000;
    contares1=0;
    contaresT=0;
    for(int i=0;i<ncfrente4;i++){
        aux=0;
        minimo=1000;
        maximo=-1000;
        theta1=atan2(nuevofrente4[i][1],nuevofrente4[i][0])*180.0/PI;
        radius=sqrt(pow(xcuad,2)+pow(ycuad,2));
        if(i==0){
            contaresT=contares;
            Xres[contares][0]=i;
            contares=contares+1;
        }
        aux=0;
        if(i>0){
            contaresT=contares;
            for(int j=0;j<contares;j++){
                if(i==Xres[j][0]){
                    aux=1;
                }
            }
        }
        if(aux==0){
            Xres[contares][0]=i;
            contares=contares+1;
            minimo=nuevofrente4[i][1];
            maximo=nuevofrente4[i][1];
            for(int j=0;j<ncfrente4;j++){
                if(j!=i){
                    radius1=sqrt(pow(nuevofrente4[i][0]-nuevofrente4[j][0],2)+pow(nuevofrente4[i][1]-nuevofrente4[j][1],2));
                    theta=atan2(nuevofrente4[j][1],nuevofrente4[j][0])*180.0/PI;
                // difftheta=sqrt(pow(theta-theta1,2));
                    if(radius1<=radius){
                        if(i==0){
                            Xres[contares][0]=j;
                            contares=contares+1;
                            if(minimo>nuevofrente4[j][1]){
                                minimo=nuevofrente4[j][1];
                            }
                            if(maximo<nuevofrente4[j][1]){
                                maximo=nuevofrente4[j][1];
                            }
                        }
                        if(i>0){
                            aux1=0;
                            for(int k=0;k<contares;k++){
                                if(j==Xres[k][0]){
                                    aux1=1;    
                                }    
                            }
                            if(aux1==0){
                                Xres[contares][0]=j;
                                contares=contares+1;
                                if(minimo>nuevofrente4[j][1]){
                                    minimo=nuevofrente4[j][1];
                                }
                                if(maximo<nuevofrente4[j][1]){
                                    maximo=nuevofrente4[j][1];
                                }
                            }   
                        }            
                    }
                }
            }

            Xres1[contares1]=contares;
            contares1=contares1+1;
            for(int j=contaresT;j<contares;j++){
                Xres[j][1]=minimo;
                Xres[j][2]=maximo;
            }
        }   

    }
// segunda parte
    aux=0;
    aux1=0;
    for(int i=0;i<contares1;i++){
            aux=1;
            aux=0;
                for(int j=0;j<contares1;j++){
                    if(j!=i){
                        xx0=sqrt(pow(Xres[Xres1[i]-1][1],2));
                        xx01=sqrt(pow(Xres[Xres1[j]-1][1],2));
                        if(xx0>xx01){
                            xx0=sqrt(pow(Xres[Xres1[i]-1][2],2));
                            aux1=1;   
                        }
                        if(xx0<xx01){
                            xx01=sqrt(pow(Xres[Xres1[j]-1][2],2));
                            aux1=2;   
                        }
                        if(xx0==xx01){
                            aux1=0;
                        }
                        if(aux1==1 || aux1==2){
                            xx02=sqrt(pow(xx0-xx01,2));
                            if(xx02<=10){
                                if(aux1==2){
                                    Xres[Xres1[i]-1][1]=Xres[Xres1[j]-1][1];
                                    Xres[Xres1[i]-1][2]=Xres[Xres1[j]-1][2];
                                }
                                aux=1;   
                            }
                        }                      
                    }
                }     
    }

    //tercera Parte
    aux1=0;
    for(int i=0;i<contares;i++){
        if(i==Xres1[aux1]){
            if(aux1<contares1){
                aux1=aux1+1;
            }
        }
        if(i<Xres1[aux1]){
            xx0=Xres[Xres1[aux1]-1][1];
            xx01=Xres[Xres1[aux1]-1][2];
            Xres[i][1]=xx0;
            Xres[i][2]=xx01;
        }
    }


//Parte final
    int ncfrente04=0;
    int nuevofrente04[cfrente4][3];    
    for(int i=0; i<ncfrente4;i++){
        varx=nuevofrente4[i][0]/xcuad;
        varx=varx*xcuad;
        varz=nuevofrente4[i][2]/zcuad;
        varz=varz*zcuad;
        for(int j=0;j<contares;j++){
            if(i==Xres[j][0]){
                vary=Xres[j][1];
                vary=vary/ycuad;
                vary=vary*ycuad;
            }
        }
        if(ncfrente04>0){
            aux=0;
            for(int j=0;j<ncfrente04;j++){
                if(nuevofrente04[j][0]==varx && nuevofrente04[j][1]==vary && nuevofrente04[j][2]==varz){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente04[ncfrente04][0]=varx;
                nuevofrente04[ncfrente04][1]=vary;
                nuevofrente04[ncfrente04][2]=varz;
                ncfrente04=ncfrente04+1;
            }
        }
        if(ncfrente04==0){
            nuevofrente04[ncfrente04][0]=varx;
            nuevofrente04[ncfrente04][1]=vary;
            nuevofrente04[ncfrente04][2]=varz;
            ncfrente04=ncfrente04+1;
        }
    }



    Puntos* puntosV=new Puntos[ncfrente01+ncfrente02+ncfrente03+ncfrente04];
    int iis=0;
    for(int i=0;i<ncfrente01;i++){
                    puntosV[iis].x=(double)  nuevofrente01[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente01[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente01[i][2]/100;
                    iis=iis+1;
    }
    for(int i=0;i<ncfrente02;i++){
                    puntosV[iis].x=(double)  nuevofrente02[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente02[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente02[i][2]/100;
                    iis=iis+1;
    }
    for(int i=0;i<ncfrente03;i++){
                    puntosV[iis].x=(double)  nuevofrente03[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente03[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente03[i][2]/100;
                    iis=iis+1;
    }
    for(int i=0;i<ncfrente04;i++){
                    puntosV[iis].x=(double)  nuevofrente04[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente04[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente04[i][2]/100;
                    iis=iis+1;
    }
    puntosV[0].n=iis;
    return  puntosV;
}*/
 int** grupos(int n, int m, int puntos1[][3],int puntos2[][3]){
    int** vectores = 0;
    vectores = new int*[n+m];
    for(int i=0;i<n+m;i++){
        vectores[i] = new int[3];
        if(i<n){
            vectores[i][0]=puntos1[i][0];
            vectores[i][1]=puntos1[i][1];
            vectores[i][2]=puntos1[i][2];
        }
        if( i-n>0 && i-n<m){
            vectores[i][0]=puntos2[i-n][0];
            vectores[i][1]=puntos2[i-n][1];
            vectores[i][2]=puntos2[i-n][2];
        }
    }
    return vectores;
}
int** gruposorg(int n,int** puntos1,int m,int ycuad,int xcuad,int zcuad){
    int** vectores = 0;
    int nuevofrente[n][3];
    int vary,varx,varz;
    int ncfrente=0;
    int aux;
   /* if(m==0){
        for(int i=0;i<n;i++){
            vary=puntos1[i][1]/ycuad;
            vary=vary*ycuad;
            varz=puntos1[i][2]/zcuad;
            varz=varz*zcuad;
            varx=puntos1[i][0];
            if(ncfrente>0){
                aux=0;
                for(int j=0;j<ncfrente;j++){
                    if(nuevofrente[j][2]==varz && nuevofrente[j][0]<varx+10 && nuevofrente[j][0]>varx-10  && nuevofrente[j][1]==vary){
                        aux=1;
                    }
                }
                if(aux==0){
                    nuevofrente[ncfrente][0]=varx;
                    nuevofrente[ncfrente][1]=vary;
                    nuevofrente[ncfrente][2]=varz;
                    ncfrente=ncfrente+1;
                }
            }else{
                    nuevofrente[ncfrente][0]=varx;
                    nuevofrente[ncfrente][1]=vary;
                    nuevofrente[ncfrente][2]=varz;
                    ncfrente=ncfrente+1;
                }
        }
    }else{
        for(int i=0;i<n;i++){
            varx=puntos1[i][0]/xcuad;
            varx=varx*xcuad;
            varz=puntos1[i][2]/zcuad;
            varz=varz*zcuad;
            vary=puntos1[i][1];
            if(ncfrente>0){
                aux=0;
                for(int j=0;j<ncfrente;j++){
                    if(nuevofrente[j][2]==varz && nuevofrente[j][1]<vary+10 && nuevofrente[j][1]>vary-10  && nuevofrente[j][0]==varx){
                        aux=1;
                    }
                }
                if(aux==0){
                    nuevofrente[ncfrente][0]=varx;
                    nuevofrente[ncfrente][1]=vary;
                    nuevofrente[ncfrente][2]=varz;
                    ncfrente=ncfrente+1;
                }
            }else{
                    nuevofrente[ncfrente][0]=varx;
                    nuevofrente[ncfrente][1]=vary;
                    nuevofrente[ncfrente][2]=varz;
                    ncfrente=ncfrente+1;
                }
        }
    }*/
    for(int i=0;i<n;i++){
        vary=puntos1[i][1]/ycuad;
        vary=vary*ycuad;
        varz=puntos1[i][2]/zcuad;
        varz=varz*zcuad;
        varx=puntos1[i][0];
        if(ncfrente>0){
            aux=0;
            for(int j=0;j<ncfrente;j++){
                if(nuevofrente[j][2]==varz && nuevofrente[j][0]<varx+10 && nuevofrente[j][0]>varx-10  && nuevofrente[j][1]==vary){
                    aux=1;
                }
            }
            if(aux==0){
                nuevofrente[ncfrente][0]=varx;
                nuevofrente[ncfrente][1]=vary;
                nuevofrente[ncfrente][2]=varz;
                ncfrente=ncfrente+1;
            }
        }else{
                nuevofrente[ncfrente][0]=varx;
                nuevofrente[ncfrente][1]=vary;
                nuevofrente[ncfrente][2]=varz;
                ncfrente=ncfrente+1;
            }
    }
    vectores = new int*[ncfrente+1];
    for(int i=0;i<ncfrente+1;i++){
        vectores[i] = new int[3];
        if(i==0){
            vectores[i][0]=ncfrente+1;
            vectores[i][1]=ncfrente+1;
            vectores[i][2]=ncfrente+1;
        }else{
            vectores[i][0]=nuevofrente[i-1][0];
            vectores[i][1]=nuevofrente[i-1][1];
            vectores[i][2]=nuevofrente[i-1][2];
        }
    }
    return vectores;
}
Puntos* organizar(Puntos* puntos, int n,double x, double y, double z){
    int varx=0,vary=0,varz=0,aux;
    int xx,yy,zz,cantx=0,canty=0,cantz=0;
    float coordrad=0,coordrad1=0,coordrad2=0,coordrad3=0,rad1=0;
    float coordreg=10,coordreg1=10,coordreg2=10,coordreg3=10,reg=0,reg1=0;
    int puntos_coord[n][3],puntos_coord1[n][3],puntos_coord2[n][3],puntos_coord3[n][3],contar=0,contar1=0,contar2=0,contar3=0;
    Voxel Cubolocal;
    double coordregor;
    Cubolocal.cantidad=0;
    int xcuad=100*x,ycuad=100*y,zcuad=100*z;   
    for(int i=0;i<n;i++){
       xx=puntos[i].x*100;
       yy=puntos[i].y*100;
       zz=puntos[i].z*100;
       rad1=sqrt(pow(xx,2)+pow(yy,2));
       if(xx<0 && yy<0){
           reg=atan2((double)yy,(double)xx);
           if(rad1>coordrad){
               coordrad=rad1;
               coordreg=reg;
           }
           puntos_coord[contar][0]=xx;
           puntos_coord[contar][1]=yy;
           puntos_coord[contar][2]=zz;
           contar=contar+1;
       }
       if(xx<0 && yy>=0){
           reg1=atan2((double)yy,(double)xx);
           if(rad1>coordrad1){
               coordrad1=rad1;
               coordreg1=reg1;
               coordregor=atan2(yy,xx);
           }
           puntos_coord1[contar1][0]=xx;
           puntos_coord1[contar1][1]=yy;
           puntos_coord1[contar1][2]=zz;
           contar1=contar1+1;
       }
       if(xx>=0 && yy<0){
           reg=atan2((double)yy,(double)xx);
           if(rad1>coordrad){
               coordrad2=rad1;
               coordreg2=reg;
           }
           puntos_coord2[contar2][0]=xx;
           puntos_coord2[contar2][1]=yy;
           puntos_coord2[contar2][2]=zz;
           contar2=contar2+1;
       }
       if(xx>=0 && yy>=0){
           reg=atan2((double)yy,(double)xx);
           if(rad1>coordrad){
               coordrad3=rad1;
               coordreg3=reg;
           }
           puntos_coord3[contar3][0]=xx;
           puntos_coord3[contar3][1]=yy;
           puntos_coord3[contar3][2]=zz;
           contar3=contar3+1;
       }
    }
    int contar01=0,contar02=0,contar11=0,contar12=0,contar21=0,contar22=0,contar31=0,contar32=0;
    int coord01[contar][3],coord02[contar][3],coord11[contar1][3],coord12[contar1][3],coord21[contar2][3],coord22[contar2][3],coord31[contar3][3],coord32[contar3][3];

    for(int i=0;i<contar;i++){
        reg=atan2((double) puntos_coord[i][1],(double) puntos_coord[i][0]);
        if(reg>=coordreg){
            coord01[contar01][0]=puntos_coord[i][0];
            coord01[contar01][1]=puntos_coord[i][1];
            coord01[contar01][2]=puntos_coord[i][2];
            contar01=contar01+1;
        }else{
            coord02[contar02][0]=puntos_coord[i][0];
            coord02[contar02][1]=puntos_coord[i][1];
            coord02[contar02][2]=puntos_coord[i][2];
            contar02=contar02+1;
        }
    }
    for(int i=0;i<contar1;i++){
        reg=atan2((double) puntos_coord1[i][1],(double) puntos_coord1[i][0]);
        if(reg>=coordreg1){
            coord11[contar11][0]=puntos_coord1[i][0];
            coord11[contar11][1]=puntos_coord1[i][1];
            coord11[contar11][2]=puntos_coord1[i][2];
            contar11=contar11+1;
        }else{
            coord12[contar12][0]=puntos_coord1[i][0];
            coord12[contar12][1]=puntos_coord1[i][1];
            coord12[contar12][2]=puntos_coord1[i][2];
            contar12=contar12+1;
        }
    }

    for(int i=0;i<contar2;i++){
        reg=atan2((double) puntos_coord2[i][1],(double) puntos_coord2[i][0]);
        if(reg>=coordreg2){
            coord21[contar21][0]=puntos_coord2[i][0];
            coord21[contar21][1]=puntos_coord2[i][1];
            coord21[contar21][2]=puntos_coord2[i][2];
            contar21=contar21+1;
        }else{
            coord22[contar22][0]=puntos_coord2[i][0];
            coord22[contar22][1]=puntos_coord2[i][1];
            coord22[contar22][2]=puntos_coord2[i][2];
            contar22=contar22+1;
        }
    }

    for(int i=0;i<contar3;i++){
        reg=atan2((double) puntos_coord3[i][1],(double) puntos_coord3[i][0]);
        if(reg>=coordreg3){
            coord31[contar31][0]=puntos_coord3[i][0];
            coord31[contar31][1]=puntos_coord3[i][1];
            coord31[contar31][2]=puntos_coord3[i][2];
            contar31=contar31+1;
        }else{
            coord32[contar32][0]=puntos_coord3[i][0];
            coord32[contar32][1]=puntos_coord3[i][1];
            coord32[contar32][2]=puntos_coord3[i][2];
            contar32=contar32+1;
        }
    }
    int** frente1= grupos(contar02,contar11,coord02,coord11);
    int** frente2= grupos(contar12,contar31,coord12,coord31);
    int** frente3= grupos(contar32,contar21,coord32,coord21);
    int** frente4= grupos(contar22,contar01,coord22,coord01);
    int** nuevofrente=gruposorg(contar02+contar11,frente1,0,ycuad,xcuad,zcuad);
    int** nuevofrente1=gruposorg(contar12+contar31,frente2,1,ycuad,xcuad,zcuad);
    int** nuevofrente2=gruposorg(contar21+contar32,frente3,0,ycuad,xcuad,zcuad);
    int** nuevofrente3=gruposorg(contar01+contar22,frente4,1,ycuad,xcuad,zcuad);
    Puntos* puntosV=new Puntos[nuevofrente[0][0]+nuevofrente1[0][0]+nuevofrente2[0][0]+nuevofrente3[0][0]];
    int iis=0;
    for(int i=1;i<nuevofrente[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente[i][2]/100;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente1[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente1[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente1[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente1[i][2]/100;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente2[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente2[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente2[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente2[i][2]/100;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente3[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente3[i][0]/100;
                    puntosV[iis].y= (double) nuevofrente3[i][1]/100;
                    puntosV[iis].z= (double) nuevofrente3[i][2]/100;
                    iis=iis+1;
    }
    puntosV[0].n=iis;
    return  puntosV;
}
main(int argc, char **argv)
{
	ros::init (argc, argv,"voxelcubo");
	ros::NodeHandle nh;
    ros::Publisher pcl_pub=nh.advertise<sensor_msgs::PointCloud2>("originalcloud",1);
    ros::Publisher pcl_pub3=nh.advertise<sensor_msgs::PointCloud2>("originalcloud2",1);
    ros::Publisher pcl_pub1=nh.advertise<sensor_msgs::PointCloud2>("limitscloud",1);
    ros::Publisher pcl_pub2=nh.advertise<sensor_msgs::PointCloud2>("voxelcloud",1);
	//ros::Publisher pcl_pub=nh.advertise<sensor_msgs::PointCloud2>("cubos",1);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud5 (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PCDReader reader;
    reader.read<pcl::PointXYZ> ("PruebasLIDAR.pcd",*cloud);
    reader.read<pcl::PointXYZ> ("PruebasLIDAR2.pcd",*cloud5);

    int tamcloud=cloud->points.size (); 
    int contador=0,conta=0,is=0;
    Puntos* punto;
    punto =new Puntos[tamcloud];
    for (int i=0;i<tamcloud;i++){
	cloud->points[i].x==cloud->points[i].x? conta=0:
		conta=1;
	if(conta==0){
		punto[is].x=cloud->points[i].x;
		punto[is].y=cloud->points[i].y;
		punto[is].z=cloud->points[i].z; 
	is++;
	}
	else{
		contador++;
	}
    }
    std::cout<<contador<<std::endl;
    Puntos* puntoss;
    Puntos* puntosvoxel;
    double xvoxel=0.1,yvoxel=0.1,zvozel=0.1;
    int maxX=2,minX=-2,maxY=2,minY=-2,maxZ=1,minZ=-1;
    puntoss=limites(punto,tamcloud,maxX,minX,maxY,minY,maxZ,minZ);
    puntosvoxel=puntoss;
    Cubo.cantidad=0;
    //puntosvoxel=voxellimit(puntoss,tamcloud,xvoxel,yvoxel,zvozel);
    puntosvoxel=organizar(puntoss,puntoss[0].n,xvoxel,yvoxel,zvozel);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
    int tamano=puntoss[0].n;
	cloud2->width=tamano;
    cloud2->height=1;
    cloud2->points.resize (cloud2->width * cloud2->height);
    std::cout<<"original: "<<tamcloud<<std::endl;
    std::cout<<"limit: "<<tamano<<std::endl;
    for (int i=0; i<tamano;i++){
        cloud2->points[i].x = puntoss[i].x;
        cloud2->points[i].y = puntoss[i].y;
        cloud2->points[i].z = puntoss[i].z;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
    tamano=puntosvoxel[0].n;
	cloud3->width=tamano;
    cloud3->height=1;
    cloud3->points.resize (cloud3->width * cloud3->height);
    std::cout<<"voxel: "<<tamano<<std::endl;
    for (int i=0; i<tamano;i++){
        cloud3->points[i].x = puntosvoxel[i].x;
        cloud3->points[i].y = puntosvoxel[i].y;
        cloud3->points[i].z = puntosvoxel[i].z;
    }
    sensor_msgs::PointCloud2 output;
    sensor_msgs::PointCloud2 output2;
    sensor_msgs::PointCloud2 output3;
    sensor_msgs::PointCloud2 output4;
    pcl::toROSMsg(*cloud,output);
    pcl::toROSMsg(*cloud2,output2);
    pcl::toROSMsg(*cloud3,output3);
    pcl::toROSMsg(*cloud5,output4);
    //pcl::toROSMsg(*cloud5,output5);
    //pcl::toROSMsg(*cloud6,output6);
	output.header.frame_id ="odom1";
    output2.header.frame_id ="odom1";
    output3.header.frame_id ="odom1";
    output4.header.frame_id ="odom1";
	std::cout<<cloud->points[72].x<<std::endl;
	std::cout<<cloud->points[72].y<<std::endl;
	std::cout<<cloud->points[72].z<<std::endl;
	ros::Rate loop_rate(1);
	while(ros::ok())
	{
		pcl_pub.publish(output);
                pcl_pub1.publish(output2);
       		pcl_pub2.publish(output3);
                pcl_pub3.publish(output4);
		ros::spinOnce();
		loop_rate.sleep();
	}
    return 0;
}
