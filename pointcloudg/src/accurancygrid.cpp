#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>
#include <Eigen/Core>
#include <unsupported/Eigen/CXX11/Tensor>

#define PI 3.14159265
struct Puntos{
 double  x, y,z;
}puntoss;
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1 (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
double minx0=-9000,miny0=0,minz0=100,maxx0=2000,maxy0=40800,maxz0=1100;
double minz11=1000,minz22=10000;
int minboxx,minboxy,minboxz,maxboxx,maxboxy,maxboxz;
int countx,county,countz;
int count00=0;
int count01=0;
int count10=0;
int count11=0;
int count20=0;
int count21=0;
void rangepoints(){
	int tamano0=cloud->points.size ();
	int tamano1=cloud1->points.size ();
	std::cout<<tamano0<<std::endl;
	std::cout<<tamano1<<std::endl;
	cloud2->width=tamano0;
    	cloud2->height=1;
    	cloud2->points.resize (cloud2->width * cloud2->height);
	for(int i=0;i<tamano0;i++){
		cloud2->points[i].x = cloud->points[i].x*1000;
        	cloud2->points[i].y = cloud->points[i].y*1000;
        	cloud2->points[i].z = cloud->points[i].z*1000;
		if(minz11>cloud2->points[i].z){
			minz11=cloud2->points[i].z;
		}
	}
	cloud3->width=tamano1;
    	cloud3->height=1;
    	cloud3->points.resize (cloud3->width * cloud3->height);
	for(int i=0;i<tamano1;i++){
		cloud3->points[i].x = cloud1->points[i].x*1000;
		cloud3->points[i].y = cloud1->points[i].y*1000;
		cloud3->points[i].z = cloud1->points[i].z*1000;	
		if(minz22>cloud3->points[i].z){
			minz22=cloud3->points[i].z;
		}
	}
}
void searchgrid(Puntos punto, int leaf){
	int tamano0=cloud2->points.size ();
	int tamano1=cloud3->points.size ();
	int contaux0=0;
	int contaux1=0;
	for(int i=0;i<tamano0;i++){
		if((cloud2->points[i].x>=punto.x && cloud2->points[i].x<punto.x+leaf)&&(cloud2->points[i].y>=punto.y && cloud2->points[i].y<punto.y+leaf)&& (cloud2->points[i].z>=punto.z && cloud2->points[i].z<punto.z+leaf)){
			contaux0=1;
			i=tamano0;
		}
	}
	if(contaux0==1){
		count01++;
	}else{
		count00++;
	}
	for(int i=0;i<tamano1;i++){
		if((cloud3->points[i].x>=punto.x && cloud3->points[i].x<punto.x+leaf)&&(cloud3->points[i].y>=punto.y && cloud3->points[i].y<punto.y+leaf)&& (cloud3->points[i].z>=punto.z && cloud3->points[i].z<punto.z+leaf)){
			contaux1=1;
			i=tamano1;
		}
	}
	if(contaux1==1){
		count11++;
	}else{
		count10++;
	}
	if(contaux0==1 && contaux1==1){
		count21++;
	}
	if(contaux0==0 && contaux1==0){
		count20++;
	}
}
void accurancysearch(int leaf){
	Puntos puntos;
        count00=0;
	count01=0;
	count10=0;
	count11=0;
	count20=0;
	count21=0;
	for(int x=0;x<countx;x++){
		for(int y=0;y<county;y++){
			for(int z=0;z<countz;z++){
				puntos.x=x;
				puntos.y=y;
				puntos.z=z;
				puntos.x*=leaf;
				puntos.y*=leaf;
				puntos.z*=leaf;
				puntos.x+=minx0;
				puntos.y+=miny0;
				puntos.z+=minz0;
				searchgrid(puntos,leaf);
			}
		}
	}
}

main(int argc, char **argv)
{
    ros::init (argc, argv,"accurancymap");
    ros::NodeHandle nh;
    ros::Publisher pcl_pub=nh.advertise<sensor_msgs::PointCloud2>("map_lidar",1);
    ros::Publisher pcl_pub1=nh.advertise<sensor_msgs::PointCloud2>("map_gazebo",1);
    ros::Publisher pcl_pub2=nh.advertise<sensor_msgs::PointCloud2>("min_point",1);
    ros::Publisher pcl_pub3=nh.advertise<sensor_msgs::PointCloud2>("max_point",1);
	//ros::Publisher pcl_pub=nh.advertise<sensor_msgs::PointCloud2>("cubos",1);
    pcl::PCDReader reader;
    reader.read<pcl::PointXYZ> ("map_design_point1.pcd",*cloud);
    reader.read<pcl::PointXYZ> ("voxel_original_map1.pcd",*cloud1);
    rangepoints();
//    std::cout<<"Minimos map lidar x: "<<minx0<<" ,y: "<<miny0<<" ,z: "<<minz0<<std::endl;
//    std::cout<<"Maximos map lidar x: "<<maxx0<<" ,y: "<<maxy0<<" ,z: "<<maxz0<<std::endl;
    double invleaf=1.0/200;
    maxboxx=floor(maxx0*invleaf);
    maxboxy=floor(maxy0*invleaf);
    maxboxz=floor(maxz0*invleaf);
    minboxx=floor(minx0*invleaf);
    minboxy=floor(miny0*invleaf);
    minboxz=floor(minz0*invleaf);
    countx=(maxboxx-minboxx+1);
    county=(maxboxy-minboxy+1);
    countz=(maxboxz-minboxz+1);
    int leaf=100;
    double accurancy;
    accurancysearch(leaf);
    accurancy=((count21+count20)*100)/(count01+count00);
    std::cout<<" Acurrancy 10cm: "<<accurancy<<std::endl;
    leaf=150;
    accurancysearch(leaf);
    accurancy=((count21+count20)*100)/(count01+count00);
    std::cout<<" Acurrancy 15cm: "<<accurancy<<std::endl;
    leaf=200;
    accurancysearch(leaf);
    accurancy=((count21+count20)*100)/(count01+count00);
    std::cout<<" Acurrancy 20cm: "<<accurancy<<std::endl;
    leaf=250;
    accurancysearch(leaf);
    accurancy=((count21+count20)*100)/(count01+count00);
    std::cout<<" Acurrancy 25cm: "<<accurancy<<std::endl;
    /*std::cout<<"Map Gazebo ones: "<<count11<<std::endl;
    std::cout<<"Map Gazebo zeros: "<<count10<<std::endl;
    std::cout<<"Map LiDAR ones: "<<count01<<std::endl;
    std::cout<<"Map LiDAR zeros: "<<count00<<std::endl;
    std::cout<<"Map Gazebo LiDAR common ones: "<<count21<<std::endl;
    std::cout<<"Map Gazebo LiDAR common zeros: "<<count20<<std::endl;*/
    
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud4 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud5 (new pcl::PointCloud<pcl::PointXYZ>);
    cloud4->width=1;
    cloud4->height=1;
    cloud4->points.resize (cloud4->width * cloud4->height);
    cloud5->width=1;
    cloud5->height=1;
    cloud5->points.resize (cloud5->width * cloud5->height);
    for(int i=0;i<1;i++){
    	cloud4->points[i].x=minx0/1000;
	cloud4->points[i].y=miny0/1000;
	cloud4->points[i].z=minz0/1000;
	cloud5->points[i].x=maxx0/1000;
	cloud5->points[i].y=maxy0/1000;
	cloud5->points[i].z=maxz0/1000;
    }
    sensor_msgs::PointCloud2 output;
    sensor_msgs::PointCloud2 output1;
    sensor_msgs::PointCloud2 output2;
    sensor_msgs::PointCloud2 output3;
    pcl::toROSMsg(*cloud,output);
    pcl::toROSMsg(*cloud1,output1);
    pcl::toROSMsg(*cloud4,output2);
    pcl::toROSMsg(*cloud5,output3);
    output.header.frame_id ="odom1";
    output1.header.frame_id ="odom1";
    output2.header.frame_id ="odom1";
    output3.header.frame_id ="odom1"; 
    //pcl::io::savePCDFileASCII ("map_design_point1.pcd", *cloud);
    //pcl::io::savePCDFileASCII ("voxel_original_map1.pcd", *cloud1);   
    ros::Rate loop_rate(1);
    while(ros::ok())
    {
	pcl_pub.publish(output);
        pcl_pub1.publish(output1);
        pcl_pub2.publish(output2);
        pcl_pub3.publish(output3);
	ros::spinOnce();
	loop_rate.sleep();
    }
    return 0;
}
