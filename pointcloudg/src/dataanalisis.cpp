#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>

#define PI 3.14159265
struct Puntos{
 double  x, y,z;
 int n,val,cuadranteC,cuadranteE;
};
struct Voxel{
 int valores [10000][4];
 int valores1 [10000][4];
 int cantidad;
}Cubo;

Puntos* limites(Puntos* punto,int n,double maxX,double minX,double maxY, double minY, double maxZ, double minZ){
    Puntos* limits=new Puntos[n];
    int ii=0;
    for (int i=0;i<n;i++){
        if((punto[i].x>minX && punto[i].x<=maxX) && (punto[i].y>minY && punto[i].y<=maxY) && (punto[i].z>minZ && punto[i].z<=maxZ)){

            limits[ii].x=punto[i].x;
            limits[ii].y=punto[i].y;
            limits[ii].z=punto[i].z;
            limits[ii].val=0;
            ii=ii+1;
        }
    }
    limits[0].n=ii;
    return limits;
}
double sec_finquadrant;
int** gruposorg(int n,int puntos1[][3],int m,int ycuad,int xcuad,int zcuad){
    int** vectores = 0;
    int cnuevar=0,cnuevar1,cnuevar2=0,nuevar[n][3],nuevar1[n][3],nuevar2[n][4];
    int varx,vary,varz;
    double diffx,diffy,diffz;
    for(int i=0;i<n;i++){
        nuevar[cnuevar][0]=puntos1[i][0];
        nuevar[cnuevar][1]=puntos1[i][1];
        nuevar[cnuevar][2]=puntos1[i][2];
        nuevar1[cnuevar][0]=puntos1[i][0];
        nuevar1[cnuevar][1]=puntos1[i][1];
        nuevar1[cnuevar][2]=puntos1[i][2];
        cnuevar=cnuevar+1;
    }
    cnuevar1=cnuevar;
    int ii=0;
    while(cnuevar1>0){
        nuevar2[cnuevar2][0]=nuevar[ii][0];
        nuevar2[cnuevar2][1]=nuevar[ii][1];
        nuevar2[cnuevar2][2]=nuevar[ii][2];
        nuevar2[cnuevar2][3]=1;
        varx=nuevar[ii][0];
        vary=nuevar[ii][1];
        varz=nuevar[ii][2];
        cnuevar1=cnuevar;
        cnuevar=0;
        for(int j=0;j<cnuevar1;j++){
            if(ii!=j){
                diffx=sqrt(pow(varx-nuevar1[j][0],2));
                diffy=sqrt(pow(vary-nuevar1[j][1],2));
                diffz=sqrt(pow(varz-nuevar1[j][2],2));
                if(diffx<xcuad && diffy<ycuad && diffz<zcuad){
                    nuevar2[cnuevar2][0]=nuevar2[cnuevar2][0]+nuevar1[j][0];
                    nuevar2[cnuevar2][1]=nuevar2[cnuevar2][1]+nuevar1[j][1];
                    nuevar2[cnuevar2][2]=nuevar2[cnuevar2][2]+nuevar1[j][2];
                    nuevar2[cnuevar2][3]=nuevar2[cnuevar2][3]+1;
                }else{
                    nuevar[cnuevar][0]=nuevar1[j][0];
                    nuevar[cnuevar][1]=nuevar1[j][1];
                    nuevar[cnuevar][2]=nuevar1[j][2];
                    cnuevar=cnuevar+1;
                }
            }
        }
        nuevar2[cnuevar2][0]=nuevar2[cnuevar2][0]/nuevar2[cnuevar2][3];
        nuevar2[cnuevar2][1]=nuevar2[cnuevar2][1]/nuevar2[cnuevar2][3];
        nuevar2[cnuevar2][2]=nuevar2[cnuevar2][2]/nuevar2[cnuevar2][3];
        cnuevar2=cnuevar2+1;
        for(int j=0;j<cnuevar;j++){
            nuevar1[j][0]=nuevar[j][0];
            nuevar1[j][1]=nuevar[j][1];
            nuevar1[j][2]=nuevar[j][2];
        }  
    }
    vectores = new int*[cnuevar2+1];
    for(int i=0;i<cnuevar2+1;i++){
        vectores[i] = new int[3];
        if(i==0){
            vectores[i][0]=cnuevar2+1;
            vectores[i][1]=cnuevar2+1;
            vectores[i][2]=cnuevar2+1;
        }else{
            vectores[i][0]=nuevar2[i-1][0];
            vectores[i][1]=nuevar2[i-1][1];
            vectores[i][2]=nuevar2[i-1][2];
        }
    }
    return vectores;
}
Puntos* organizar(Puntos* puntos, int n,double x, double y, double z,int numobs){
    int xx,yy,zz,cuad1=0,cuad2=0,cuad3=0,cuad4=0;
    int xcuad=x*1000,ycuad=y*1000,zcuad=z*1000;
    int cuadrante1[n][3],cuadrante2[n][3],cuadrante3[n][3],cuadrante4[n][3];
    for(int i=0;i<n;i++){
        xx=puntos[i].x*1000;
        yy=puntos[i].y*1000;
        zz=puntos[i].z*1000;
        if(zz>-380){
            if(xx>=0 && yy>=0){
                cuadrante1[cuad1][0]=xx;
                cuadrante1[cuad1][1]=yy;
                cuadrante1[cuad1][2]=zz;
                cuad1=cuad1+1;
            }
            if(xx<0 && yy>=0){
                cuadrante2[cuad2][0]=xx;
                cuadrante2[cuad2][1]=yy;
                cuadrante2[cuad2][2]=zz;
                cuad2=cuad2+1;
            }
            if(xx<0 && yy<0){
                cuadrante3[cuad3][0]=xx;
                cuadrante3[cuad3][1]=yy;
                cuadrante3[cuad3][2]=zz;
                cuad3=cuad3+1;
            }
            if(xx>=0 && yy<0){
                cuadrante4[cuad4][0]=xx;
                cuadrante4[cuad4][1]=yy;
                cuadrante4[cuad4][2]=zz;
                cuad4=cuad4+1;
            }
        }
    }
 //   int** frente1= grupos(contar02,contar11,coord02,coord11);
    int anglec;
    sec_finquadrant=ros::Time::now().toSec();
    Puntos* puntosV=new Puntos[n];
    std::cout<<" cuad 1 size: "<<cuad1<<std::endl;
    std::cout<<" cuad 2 size: "<<cuad2<<std::endl;
    std::cout<<" cuad 3 size: "<<cuad3<<std::endl;
    std::cout<<" cuad 4 size: "<<cuad4<<std::endl;
    int iis=0;
    if(cuad1>0){
        anglec=1;
        int** nuevofrente1=gruposorg(cuad1,cuadrante1,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente1[0][0];i++){
                        puntosV[iis].x=(double)  nuevofrente1[i][0]/1000;
                        puntosV[iis].y= (double) nuevofrente1[i][1]/1000;
                        puntosV[iis].z= (double) nuevofrente1[i][2]/1000;
                        iis=iis+1;
        }
    }
    if(cuad2>0){
        anglec=-1;
        int** nuevofrente2=gruposorg(cuad2,cuadrante2,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente2[0][0];i++){
                        puntosV[iis].x=(double)  nuevofrente2[i][0]/1000;
                        puntosV[iis].y= (double) nuevofrente2[i][1]/1000;
                        puntosV[iis].z= (double) nuevofrente2[i][2]/1000;
                        iis=iis+1;
        }
    }
    if(cuad3>0){
        anglec=1;
        int** nuevofrente3=gruposorg(cuad3,cuadrante3,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente3[0][0];i++){
                        puntosV[iis].x=(double)  nuevofrente3[i][0]/1000;
                        puntosV[iis].y= (double) nuevofrente3[i][1]/1000;
                        puntosV[iis].z= (double) nuevofrente3[i][2]/1000;
                        iis=iis+1;
        }
    }
    if(cuad4>0){
        anglec=-1;
        int** nuevofrente4=gruposorg(cuad4,cuadrante4,anglec,ycuad,xcuad,zcuad);
        for(int i=1;i<nuevofrente4[0][0];i++){
                        puntosV[iis].x=(double)  nuevofrente4[i][0]/1000;
                        puntosV[iis].y= (double) nuevofrente4[i][1]/1000;
                        puntosV[iis].z= (double) nuevofrente4[i][2]/1000;
                        iis=iis+1;
        }
    }
    int nuevar[iis][4],cnuevar=0,nuevar1[iis][4],cnuevar1=0,nuevar2[iis][4],cnuevar2=0,conteo=2;
    float diffx,diffy,diffz,diffr,diffr1;
    int varx,vary,varz;
    for(int i=0;i<iis;i++){
        nuevar[cnuevar][0]=puntosV[i].x*1000;
        nuevar[cnuevar][1]=puntosV[i].y*1000;
        nuevar[cnuevar][2]=puntosV[i].z*1000;
        nuevar1[cnuevar][0]=puntosV[i].x*1000;
        nuevar1[cnuevar][1]=puntosV[i].y*1000;
        nuevar1[cnuevar][2]=puntosV[i].z*1000;
        cnuevar=cnuevar+1;
    }
    cnuevar1=cnuevar;
    int numobj=1+numobs;
    while(cnuevar1>0){
        for(int i=0;i<conteo;i++){
            if(i==0){
                varx=nuevar[i][0];
                vary=nuevar[i][1];
                varz=nuevar[i][2];
                nuevar2[cnuevar2][0]=nuevar[i][0];
                nuevar2[cnuevar2][1]=nuevar[i][1];
                nuevar2[cnuevar2][2]=nuevar[i][2];
                nuevar2[cnuevar2][3]=numobj;
                cnuevar2=cnuevar2+1;
            }else{
                varx=nuevar2[i][0];
                vary=nuevar2[i][1];
                varz=nuevar2[i][2];
            }
            cnuevar1=cnuevar;
            cnuevar=0;
            for(int j=0;j<cnuevar1;j++){
                if((i==0 && i!=j)||(i>0)){
                    diffx=sqrt(pow(varx-nuevar1[j][0],2));
                    diffy=sqrt(pow(vary-nuevar1[j][1],2));
                    diffz=sqrt(pow(varz-nuevar1[j][2],2));
                    if(diffx<=xcuad*4 && diffy<=ycuad*4 && diffz<=zcuad*4){
                        nuevar2[cnuevar2][0]=nuevar1[j][0];
                        nuevar2[cnuevar2][1]=nuevar1[j][1];
                        nuevar2[cnuevar2][2]=nuevar1[j][2];
                        nuevar2[cnuevar2][3]=numobj;
                        cnuevar2=cnuevar2+1;
                    } else{
                        nuevar[cnuevar][0]=nuevar1[j][0];
                        nuevar[cnuevar][1]=nuevar1[j][1];
                        nuevar[cnuevar][2]=nuevar1[j][2]; 
                        cnuevar=cnuevar+1; 
                    }             
                }
            }
            for(int j=0;j<cnuevar;j++){
                nuevar1[j][0]=nuevar[j][0];
                nuevar1[j][1]=nuevar[j][1];
                nuevar1[j][2]=nuevar[j][2]; 
            }
            if(cnuevar1==0){
                i=conteo;
            }
            conteo=cnuevar2;
        }
        numobj=numobj+1;
    }
    Puntos* puntosV1=new Puntos[cnuevar2];
    iis=0;
    for(int i=0;i<cnuevar2;i++){
        puntosV1[iis].x=(double) nuevar2[i][0]/1000;
        puntosV1[iis].y=(double) nuevar2[i][1]/1000;
        puntosV1[iis].z=(double) nuevar2[i][2]/1000;
        puntosV1[iis].val=nuevar2[i][3];
        iis=iis+1;
    }
    std::cout<<numobj-1<<std::endl;
    puntosV1[0].n=iis;
    return  puntosV1;
}
double sec_now;
double sec_finlimit;
double sec_finvoxel;
double sec_fininitmap;
double sec_finvalpos;
double sec_finupdatepos;
main(int argc, char **argv)
{
	ros::init (argc, argv,"voxelcubo");
	ros::NodeHandle nh;
    ros::Publisher pcl_pub=nh.advertise<sensor_msgs::PointCloud2>("originalcloud",1);
    ros::Publisher pcl_pub1=nh.advertise<sensor_msgs::PointCloud2>("limitscloud",1);
    ros::Publisher pcl_pub2=nh.advertise<sensor_msgs::PointCloud2>("voxelcloud",1);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PCDReader reader;
    reader.read<pcl::PointXYZ> ("PruebasCarro2.pcd",*cloud);

    int tamcloud=cloud->points.size ();
    Puntos* punto;
    punto =new Puntos[tamcloud];
    for (int i=0;i<tamcloud;i++){
        punto[i].x=cloud->points[i].x;
        punto[i].y=cloud->points[i].y;
        punto[i].z=cloud->points[i].z; 
    }
    Puntos* puntoss;
    Puntos* puntosvoxel;
    double xvoxel=0.1,yvoxel=0.1,zvozel=0.1;
    double maxX=2,minX=-2,maxY=2,minY=-2,maxZ=1,minZ=-1;
    puntoss=limites(punto,tamcloud,maxX,minX,maxY,minY,maxZ,minZ);
    puntosvoxel=puntoss;
    Cubo.cantidad=0;
    sec_now=ros::Time::now().toSec();
    puntosvoxel=organizar(puntoss,puntoss[0].n,xvoxel,yvoxel,zvozel,0);
    sec_finlimit=ros::Time::now().toSec();
    std::cout<<"Time execution time: "<<sec_finlimit-sec_now<<endl;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
    int tamano=puntoss[0].n;
	cloud2->width=tamano;
    cloud2->height=1;
    cloud2->points.resize (cloud2->width * cloud2->height);
    std::cout<<"original: "<<tamcloud<<std::endl;
    std::cout<<"limit: "<<tamano<<std::endl;
    for (int i=0; i<tamano;i++){
        cloud2->points[i].x = puntoss[i].x;
        cloud2->points[i].y = puntoss[i].y;
        cloud2->points[i].z = puntoss[i].z;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
    tamano=puntosvoxel[0].n;
	cloud3->width=tamano;
    cloud3->height=1;
    cloud3->points.resize (cloud3->width * cloud3->height);
    std::cout<<"voxel: "<<tamano<<std::endl;
    for (int i=0; i<tamano;i++){
            cloud3->points[i].x = puntosvoxel[i].x;
            cloud3->points[i].y = puntosvoxel[i].y;
            cloud3->points[i].z = puntosvoxel[i].z;
    }
    sensor_msgs::PointCloud2 output;
    sensor_msgs::PointCloud2 output2;
    sensor_msgs::PointCloud2 output3;
    pcl::toROSMsg(*cloud,output);
    pcl::toROSMsg(*cloud2,output2);
    pcl::toROSMsg(*cloud3,output3);
	output.header.frame_id ="odom1";
    output2.header.frame_id ="odom1";
    output3.header.frame_id ="odom1";
	ros::Rate loop_rate(1);
	while(ros::ok())
	{
		pcl_pub.publish(output);
        pcl_pub1.publish(output2);
        pcl_pub2.publish(output3);
		ros::spinOnce();
		loop_rate.sleep();
	}
    return 0;
}


 /*int** grupos(int n, int m, int puntos1[][3],int puntos2[][3]){
    int** vectores = 0;
    vectores = new int*[n+m];
    for(int i=0;i<n+m;i++){
        vectores[i] = new int[3];
        if(i<n){
            vectores[i][0]=puntos1[i][0];
            vectores[i][1]=puntos1[i][1];
            vectores[i][2]=puntos1[i][2];
        }
        if( i-n>0 && i-n<m){
            vectores[i][0]=puntos2[i-n][0];
            vectores[i][1]=puntos2[i-n][1];
            vectores[i][2]=puntos2[i-n][2];
        }
    }
    return vectores;
}*/
/*int** gruposorg(int n,int puntos1[][3],int m,int ycuad,int xcuad,int zcuad){
    int** vectores = 0;
    int nuevofrente[n][7],nuevofrente1[n][4],nuevofrentex[n][50],nuevofrentey[n][50],nuevofrentez[n][50];
    int vary,varx,varz,varz1,varz2;
    float r,r1,r2,xx1,yy1,xx2,yy2;
    float diffz1,diffz2,diffz3,diffz4,diffz5,theta,theta2,theta3;
    int ncfrente=0;
    int aux;
    double anglesr= (PI/4)*m;
    for(int i=0;i<n;i++){
        varz=puntos1[i][2]/zcuad;
        varz=varz*zcuad;
        varz1=varz+zcuad;
        varx=puntos1[i][0];
        varx=varx/xcuad;
        varx=varx*xcuad;
        vary=puntos1[i][1];
        vary=vary/ycuad;
        vary=vary*ycuad;
        if(ncfrente>0){
            aux=0;
            for(int j=0;j<ncfrente;j++){
                r1=sqrt(pow(nuevofrente[j][0]-varx,2)+pow(nuevofrente[j][1]-vary,2));
                r=sqrt(pow(xcuad,2)+pow(ycuad,2));
                if(r1<=r && varz==nuevofrente[j][2]){
                    aux=1;
                    nuevofrente[j][3]=nuevofrente[j][3]+1;
                    nuevofrentex[j][nuevofrentex[j][0]]=puntos1[i][0];
                    nuevofrentey[j][nuevofrentey[j][0]]=puntos1[i][1];
                    nuevofrentez[j][nuevofrentez[j][0]]=varz;
                    nuevofrentex[j][0]=nuevofrentex[j][0]+1;
                    nuevofrentey[j][0]=nuevofrentey[j][0]+1;
                    nuevofrentez[j][0]=nuevofrentez[j][0]+1;
                    //OPcion1
              //      nuevofrente[j][4]=nuevofrente[j][4]+varx;
              //      nuevofrente[j][5]=nuevofrente[j][5]+vary;
              //      nuevofrente[j][6]=nuevofrente[j][6]+varz;
                    //OPcion 2
                    nuevofrente[j][4]=nuevofrente[j][4]+puntos1[i][0];
                    nuevofrente[j][5]=nuevofrente[j][5]+puntos1[i][1];
                    nuevofrente[j][6]=nuevofrente[j][6]+varz;
                }
            }
            if(aux==0){
                nuevofrente[ncfrente][0]=varx;
                nuevofrente[ncfrente][1]=vary;
                nuevofrente[ncfrente][2]=varz;
                nuevofrente[ncfrente][3]=1;
                 // Opcion 1
               // nuevofrente[ncfrente][4]=varx;
               // nuevofrente[ncfrente][5]=vary;
               // nuevofrente[ncfrente][6]=varz;
                // Opcion 2
                nuevofrente[ncfrente][4]=puntos1[i][0];
                nuevofrente[ncfrente][5]=puntos1[i][1];
                nuevofrente[ncfrente][6]=varz;
                nuevofrentex[ncfrente][0]=2;
                nuevofrentey[ncfrente][0]=2;
                nuevofrentez[ncfrente][0]=2;
                nuevofrentex[ncfrente][1]=puntos1[i][0];
                nuevofrentey[ncfrente][1]=puntos1[i][1];
                nuevofrentez[ncfrente][1]=varz;       
                ncfrente=ncfrente+1;
            }
        }else{
                nuevofrente[ncfrente][0]=varx;
                nuevofrente[ncfrente][1]=vary;
                nuevofrente[ncfrente][2]=varz;
                nuevofrente[ncfrente][3]=1;
                // Opcion 1
            //    nuevofrente[ncfrente][4]=varx;
            //    nuevofrente[ncfrente][5]=vary;
            //    nuevofrente[ncfrente][6]=varz;
                // Opcion 2
                nuevofrente[ncfrente][4]=puntos1[i][0];
                nuevofrente[ncfrente][5]=puntos1[i][1];
                nuevofrente[ncfrente][6]=varz;
                nuevofrentex[ncfrente][0]=2;
                nuevofrentey[ncfrente][0]=2;
                nuevofrentez[ncfrente][0]=2;
                nuevofrentex[ncfrente][1]=puntos1[i][0];
                nuevofrentey[ncfrente][1]=puntos1[i][1];
                nuevofrentez[ncfrente][1]=varz;
                ncfrente=ncfrente+1;
            }
    }
    int xx,yy,zz;
    for(int i=0;i<ncfrente;i++){
        nuevofrente[i][0]=nuevofrente[i][4]/nuevofrente[i][3]; 
        nuevofrente[i][1]=nuevofrente[i][5]/nuevofrente[i][3];
        nuevofrente[i][2]=nuevofrente[i][6]/nuevofrente[i][3];
    }
    vectores = new int*[ncfrente+1];
    for(int i=0;i<ncfrente+1;i++){
        vectores[i] = new int[3];
        if(i==0){
            vectores[i][0]=ncfrente+1;
            vectores[i][1]=ncfrente+1;
            vectores[i][2]=ncfrente+1;
        }else{
            vectores[i][0]=nuevofrente[i-1][0];
            vectores[i][1]=nuevofrente[i-1][1];
            vectores[i][2]=nuevofrente[i-1][2];
        }
    }
    return vectores;
}*/
/*Puntos* organizar(Puntos* puntos, int n,double x, double y, double z){
    int xx,yy,zz,cuad1=0,cuad2=0,cuad3=0,cuad4=0;
    int xcuad=x*1000,ycuad=y*1000,zcuad=z*1000;
    int cuadrante1[n][3],cuadrante2[n][3],cuadrante3[n][3],cuadrante4[n][3];
    for(int i=0;i<n;i++){
        xx=puntos[i].x*1000;
        yy=puntos[i].y*1000;
        zz=puntos[i].z*1000;
        if(zz>-380){
            if(xx>=0 && yy>=0){
                cuadrante1[cuad1][0]=xx;
                cuadrante1[cuad1][1]=yy;
                cuadrante1[cuad1][2]=zz;
                cuad1=cuad1+1;
            }
            if(xx<0 && yy>=0){
                cuadrante2[cuad2][0]=xx;
                cuadrante2[cuad2][1]=yy;
                cuadrante2[cuad2][2]=zz;
                cuad2=cuad2+1;
            }
            if(xx<0 && yy<0){
                cuadrante3[cuad3][0]=xx;
                cuadrante3[cuad3][1]=yy;
                cuadrante3[cuad3][2]=zz;
                cuad3=cuad3+1;
            }
            if(xx>=0 && yy<0){
                cuadrante4[cuad4][0]=xx;
                cuadrante4[cuad4][1]=yy;
                cuadrante4[cuad4][2]=zz;
                cuad4=cuad4+1;
            }
        }
    }*/
 //   int** frente1= grupos(contar02,contar11,coord02,coord11);
 /*   int anglec=1;
    int** nuevofrente1=gruposorg(cuad1,cuadrante1,anglec,ycuad,xcuad,zcuad);
    anglec=-1;
    int** nuevofrente2=gruposorg(cuad2,cuadrante2,anglec,ycuad,xcuad,zcuad);
    anglec=1;
    int** nuevofrente3=gruposorg(cuad3,cuadrante3,anglec,ycuad,xcuad,zcuad);
    anglec=-1;
    int** nuevofrente4=gruposorg(cuad4,cuadrante4,anglec,ycuad,xcuad,zcuad);
    std::cout<<"hoal"<<std::endl;
    Puntos* puntosV=new Puntos[nuevofrente1[0][0]+nuevofrente2[0][0]+nuevofrente3[0][0]+nuevofrente4[0][0]];
    int iis=0;
    for(int i=1;i<nuevofrente1[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente1[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente1[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente1[i][2]/1000;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente2[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente2[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente2[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente2[i][2]/1000;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente3[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente3[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente3[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente3[i][2]/1000;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente4[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente4[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente4[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente4[i][2]/1000;
                    iis=iis+1;
    }
    puntosV[0].n=iis;
    return  puntosV;
}*/
