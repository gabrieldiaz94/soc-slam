#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>
#include <nav_msgs/Odometry.h>
#include "sensor_msgs/Imu.h"
#define PI 3.14159265
int carrito=0;
int carrito1=0;
sensor_msgs::PointCloud2 entrada;
void cloudCB(const sensor_msgs::PointCloud2 &input)
{ 
    entrada=input;
    carrito=1;    
}
double sec_now;
double sec_finlimit;
double sec_finvoxel;
double sec_fininitmap;
double sec_finvalpos;
double sec_finupdatepos;
double inicio_s;
double fin_s;
main (int argc, char **argv)
{
    ros::init (argc, argv, "get_time");
    ros::NodeHandle nh;
    inicio_s=ros::Time::now().toSec();
    ros::Subscriber bat_sub=nh.subscribe("/hdl_graph_slam/map_points",64,cloudCB);
    //ros::Subscriber bat_sub=nh.subscribe("/rs_points",64,cloudCB);
  //  ros::spin();
    while(ros::ok())
	{ 
        if(carrito==1 ){
 		fin_s=ros::Time::now().toSec();
		std::cout<<"map_time: "<<fin_s-inicio_s<<std::endl;
		inicio_s=ros::Time::now().toSec();
	 	carrito=0;

        }      
		ros::spinOnce();
			
	}   
	return 0;
}
